function confirmBookingSlotWidget(event){
    event.preventDefault();
    clearMessages();
    startWorking();
    var appointmentModel = model();
    Meteor.call('bookAppointment', appointmentModel, function(err,response) {
        if(err) {
            Session.set('errorMessage',err.message);
        }else{
            appointmentModel._id = response;
            model(appointmentModel);
            Router.go("/thanks/"+appointmentModel._id);
        }
        stopWorking();
    });

}
function clearSlot(){
    setValue("selectedSlot",null);
    appointmentModel = model();
    delete appointmentModel.slotId;
    delete appointmentModel.slotIndex;
    delete appointmentModel.slotDate;
    delete appointmentModel.slotStart;
    delete appointmentModel.slotEnd;
    model(appointmentModel);
}
function bookAppointment(){
    startWorking();
    var givenAppointment = model();
    Meteor.call('bookAppointment', givenAppointment, function(err,response) {
        if(err) {
            console.log(err);
            Session.set('serverDataResponse', "Error:" + err.reason);
            return;
        }
        stopWorking(err);
        if(response){
            Router.go('thanks');
        }
    });

}
function serviceIndex(serviceName){
    serviceDetails = getValue("serviceDetails");
    foundIndex=-1;
    if(serviceDetails){
        for(var index=0;index<serviceDetails.length;index++){
            if(serviceName === serviceDetails[index].name){
                foundIndex=index;
                break;
            }
        }
    }
    return foundIndex;
}
function serviceGroupIndex(serviceGroupName){
    serviceDetails = getValue("serviceDetails");
    var foundIndex=-1;
    if(serviceDetails){
        for(var index=0;index<serviceDetails.length;index++){
            for(var innerIndex=0;innerIndex<serviceDetails[index].groups.length;innerIndex++) {
                if (serviceGroupName === serviceDetails[index].groups[innerIndex]) {
                    foundIndex = index;
                    break;
                }
            }
        }
    }
    return foundIndex;
}
function deSelectService(serviceName){
    serviceDetails = getValue("serviceDetails");
    foundIndex=serviceIndex(serviceName);
    if(foundIndex!= -1)
    {
        serviceDetails.splice(foundIndex,1);
    }
    setValue('serviceDetails',serviceDetails);
    if(getValue("serviceDetails") && getValue("serviceDetails").length ==0 ){
        setValue("selectedSlot",undefined);
    }
}
/**
 * select a service in a group removing any other
 * selections in the same group
 * @param service
 * @param otherServiceNames
 */
function selectService(service,otherServiceNames,allowMultiple){
    //deselect other services
    if(!allowMultiple) {
        for (var index = 0; index < otherServiceNames.length; index++) {
            deSelectService(otherServiceNames[index]);
        }
    }
    serviceDetails = getValue("serviceDetails");
    if(!serviceDetails){
        serviceDetails = [];
    }
    serviceDetails.push(service);
    setValue('serviceDetails', serviceDetails);
}

sumReducer = function(memo, num){
    return memo + num;
};
durationMapper =  function(serviceDetail){
    if (serviceDetail.durationInMinutes)
        return Number(serviceDetail.durationInMinutes);
    else
        return 0;
}
priceMapper =  function(serviceDetail){
    if (serviceDetail.priceFrom)
        return Number(serviceDetail.priceFrom);
    else
        return 0;
}
Template.book.events = {
    'click .confirmBookingSlotWidget': confirmBookingSlotWidget
};

Template.book.rendered = function(){
  Template.contactDetailsSidebar.rendered();
  Template.serviceDetailsSidebar.rendered();
  Template.slotsSidebar.rendered();
  Template.reserveDetailsSidebar.rendered();
}

Template.serviceDetailsSidebar.rendered = function(){
    serviceDetailsSidebar = $('.serviceDetailsSidebar').first().sidebar('setting', 'transition', 'overlay');
    if($(".serviceDetailsHandle").length) {
        serviceDetailsSidebar.sidebar('attach events', '.serviceDetailsHandle');
    }
    formRules = {
        serviceDetails:{
            identifier  : 'serviceDetails',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please select at least one service'
                }
            ]}
    };
    onSuccessCallback = function() {
        serviceDetailsSidebar.sidebar("toggle");
        $(".serviceDetailsHandle .ui.message").removeClass("warning error").addClass("success");
        return false
    };
    onFailureCallback = function() {
        $(".serviceDetailsHandle .ui.message").removeClass("warning success").addClass("error");
        return false;
    };
    formOptions = {
        inline: false,
        on: 'blur',
        onSuccess: onSuccessCallback,
        onFailure: onFailureCallback
    };

    $('.serviceDetailsForm').form(_.extend({fields:formRules},formOptions));
    $(".ui.accordion.serviceTypes").accordion();
};
Template.serviceDetailsSidebar.events({
    'click p.serviceType': function () {
        if(Session.get("selected_serviceType") && Session.get("selected_serviceType")._id != this._id ){
            Session.set("selected_serviceDetail", undefined);
            setValue("serviceDetail",undefined);
        }
        setValue('serviceType',this);
        Session.set("selected_serviceType", this);

    },
    'click .ui.buttons .button':function(event,template){
        if ($(event.currentTarget).hasClass('active')){
            $(event.currentTarget).removeClass('active');
            deSelectService(this.name);
            // deselect active parent
            othersAreActive = false;
            $(event.currentTarget).siblings().each(function(){
                othersAreActive = othersAreActive || otherServiceNames.push($(this).hasClass("active"));
            });
            if(!othersAreActive) {
                $(event.currentTarget).closest(".content").prev(".title").removeClass('teal').children().removeClass('teal');
            }
        }else{
            otherServiceNames=[];
            $(event.currentTarget).siblings().each(function(){
                otherServiceNames.push($(this).attr("name"));
            });
            this.groupDescription = $(event.currentTarget).closest(".content").prev(".title").text().trim();
            targetGroup = ServiceGroups.findOne({title:this.groupDescription});
            selectService(this,otherServiceNames,targetGroup && targetGroup.allowMultiple);
            if(targetGroup && !targetGroup.allowMultiple)
               $(event.currentTarget).addClass('active').siblings().removeClass('active');
            //select active parent
            $(event.currentTarget).closest(".content").prev(".title").addClass('teal').children().addClass('teal');
        }
        clearSlot();
    }
});



Template.serviceDetailsSidebar.helpers({
   serviceGroupSelected : function () {
        var serviceHasBeenSelected=false;
        var serviceDetails = getValue("serviceDetails");
        if (serviceDetails && serviceGroupIndex(this.name) != -1) {
            serviceHasBeenSelected = true;
        }

        return serviceHasBeenSelected  ? "teal" : '';
    },
    serviceDetailSelected:function () {
        return serviceIndex(this.name)!= -1 ? "active" : '';
    },
    serviceGroups : function () {
        return ServiceGroups.find({});
    },
    serviceTypes : function(){
      return ServiceTypes.find({groups:this.name});
    }
});

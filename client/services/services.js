import {getRequiredDate} from '../../lib/slotHelpers';
import {setRequiredDate} from '../../lib/slotHelpers';
import {Booking, undoBooking, opad} from '../../lib/slotHelpers';



Template.services.helpers({
    services: function(){
        return ServiceTypes.find();
    },
});

Template.serviceGroups.helpers({
    services: function(){
        return ServiceGroups.find();
    },
});

Template.serviceConfigs.helpers({
    services: function(){
        return ServiceConfig.find();
    },
    opad,
});



Template.services.events = {
    'click .editWidget': function(event){
        $(".addServiceHandle .ui.message").removeClass("warning success error");
        $('.addServiceForm').form('clear', this);
        $('.addServiceForm').form('set values', this);
    },
    'click .deleteWidget': function () {
        const id = this._id;
        $('.todelete').text(this.name);
        $('.ui.basic.modal').
        modal({
              closable  : false,
              onDeny    : function(){
                // do nothing
              },
              onApprove : function() {
                ServiceTypes.remove({_id: id});
              }
        }).
        modal('show');
    }
};



Template.servicesLayout.events = {
    'click .addServiceWidget': function(event){
        $(".addServiceHandle .ui.message").removeClass("warning success error");
        $('.addServiceForm').form('clear', this);
    },
    'click .addServiceGroupWidget': function(event){
        $(".addServiceGroupHandle .ui.message").removeClass("warning success error");
        $('.addServiceGroupForm').form('clear', this);
    },

};

Template.serviceGroups.events = {
    'click .editWidget': function(event){
        $(".addServiceGroupHandle .ui.message").removeClass("warning success error");
        $('.addServiceGroupForm').form('clear', this);
        $('.addServiceGroupForm').form('set values', this);
    },
    'click .deleteWidget': function () {
        const id = this._id;
        $('.todelete').text(this.name);
        $('.ui.basic.modal').
        modal({
              closable  : false,
              onDeny    : function(){
                // do nothing
              },
              onApprove : function() {
                ServiceGroups.remove({_id: id});
              }
        }).
        modal('show');
    }
};
Template.serviceConfigs.events = {
    'click .editWidget': function(event){
        $(".addServiceConfigHandle .ui.message").removeClass("warning success error");
        $('.addServiceConfigForm').form('clear', this);
        if(this.from) {
          this.fromhour = opad(this.from.hour);
          this.fromminute = opad(this.from.minute);
        }
        if(this.to) {
          this.tohour = opad(this.to.hour);
          this.tominute = opad(this.to.minute);
        }
        $('.addServiceConfigForm').form('set values', this);
    },
  };

Template.addServiceSidebar.helpers({
    serviceGroups: function(){
        return ServiceGroups.find();
    },
});


Template.addServiceConfigSidebar.helpers({
    hours: function(){
        return _.range(24);
    },
    minutes: function(){
        return _.range(0, 60, 5);
    },
    opad,
});

Template.services.rendered= function(){
    Template.addServiceSidebar.rendered();
    Template.addServiceGroupSidebar.rendered();
}

Template.serviceGroups.rendered= function(){
    Template.addServiceGroupSidebar.rendered();
}

Template.serviceConfigs.rendered= function(){
    Template.addServiceConfigSidebar.rendered();
}


Template.addServiceSidebar.rendered = function(){
    const addServiceSidebar = $('.addServiceSidebar').first().sidebar('setting', 'transition', 'overlay');
    if($(".addServiceSidebar").length) {
        addServiceSidebar.sidebar('attach events', '.addServiceHandle');
    }
    $('.ui.dropdown').dropdown();
    formRules = {
        name:{
            identifier  : 'name',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please provide a name for the service'
                }
            ]},
        description:{
            identifier  : 'description',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please provide a description for the service'
                }
            ]},
        groups:{
            identifier  : 'groups',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please provide a group for the service'
                }
            ]},
        durationInMinutes:{
            identifier  : 'durationInMinutes',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please provide duration in minutes for the service',
                },
                {
                    type   : 'integer',
                    prompt : 'duration needs to be a number in minutes',
                }
            ]},
        priceFrom:{
            identifier  : 'priceFrom',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please provide price in pounds for the service',
                },
                {
                    type   : 'number',
                    prompt : 'duration needs to be a number',
                }
            ]}
    };
    onSuccessCallback = function() {
        startWorking();
        const m = $('.addServiceForm').form('get values');
        m.priceFrom = Number(m.priceFrom);
        m.durationInMinutes = Number(m.durationInMinutes);
        let targetMethod = 'addServiceType';
        if (m._id && m._id.length > 0) {
          targetMethod = 'updateServiceType';
        }
        Meteor.call(targetMethod, m, function(err,response) {
            if(err) {
                Session.set('serverDataResponse', "Error:" + err.reason);
                return;
            }
            stopWorking(err);
            if(response){
                $(".addServiceHandle .ui.message").removeClass("warning success").addClass("error");
                addServiceSidebar.sidebar("toggle");
            }
        });
        return false
    };
    onFailureCallback= function() {
        $(".addServiceHandle .ui.message").removeClass("warning success").addClass("error");
        return false;
    };
    formOptions= {
        inline: true,
        on: 'blur',
        onSuccess: onSuccessCallback,
        onFailure: onFailureCallback
    };

    $('.addServiceForm').form(_.extend({fields:formRules},formOptions));

};
Template.addServiceGroupSidebar.rendered = function(){
    const addServiceGroupSidebar = $('.addServiceGroupSidebar').first().sidebar('setting', 'transition', 'overlay');
    if($(".addServiceGroupSidebar").length) {
        addServiceGroupSidebar.sidebar('attach events', '.addServiceGroupHandle');
    }
    $('.ui.dropdown').dropdown();
    formRules = {
        name:{
            identifier  : 'name',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please provide a name for the service group'
                }
            ]},
        description:{
            identifier  : 'description',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please provide a description for the service group'
                }
            ]},
        title:{
            identifier  : 'title',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please provide a title for the service group'
                }
            ]},
    };
    onSuccessCallback = function() {
        startWorking();
        const m = $('.addServiceGroupForm').form('get values');
        m.allowMultiple = m.allowMultiple === 'on' ? true : false;
        let targetMethod = 'addServiceGroup';
        if (m._id && m._id.length > 0) {
          targetMethod = 'updateServiceGroup';
        }
        Meteor.call(targetMethod, m, function(err,response) {
            if(err) {
                Session.set('serverDataResponse', "Error:" + err.reason);
                return;
            }
            stopWorking(err);
            if(response){
                $(".addServiceGroupHandle .ui.message").removeClass("warning success").addClass("error");
                addServiceGroupSidebar.sidebar("toggle");
            }
        });
        return false
    };
    onFailureCallback= function() {
        $(".addServiceGroupHandle .ui.message").removeClass("warning success").addClass("error");
        return false;
    };
    formOptions= {
        inline: true,
        on: 'blur',
        onSuccess: onSuccessCallback,
        onFailure: onFailureCallback
    };

    $('.addServiceGroupForm').form(_.extend({fields:formRules},formOptions));

};

Template.addServiceConfigSidebar.rendered = function(){
    const addServiceConfigSidebar = $('.addServiceConfigSidebar').first().sidebar('setting', 'transition', 'overlay');
    if($(".addServiceConfigSidebar").length) {
        addServiceConfigSidebar.sidebar('attach events', '.addServiceConfigHandle');
    }
    $('.ui.dropdown').dropdown();
    formRules = {
        dayText:{
            identifier  : 'dayText',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please provide the day'
                }
            ]},

    };
    onSuccessCallback = function() {
        startWorking();
        const m = $('.addServiceConfigForm').form('get values');
        console.log(m);
        m.open = m.open === 'on' ? true : false;
        m.day = Number(m.day);
        if (m.open) {
          m.from = {hour: Number(m.fromhour), minute: Number(m.fromminute)};
          m.to = {hour: Number(m.tohour), minute: Number(m.tominute)};
        } else {
          m.from = null;
          m.to = null;
        }
        delete m.fromhour;
        delete m.fromminute;
        delete m.tohour;
        delete m.tominute;
        console.log(m);
        let targetMethod = 'addServiceConfig';
        if (m._id && m._id.length > 0) {
          targetMethod = 'updateServiceConfig';
        }
        Meteor.call(targetMethod, m, function(err,response) {
            if(err) {
                Session.set('serverDataResponse', "Error:" + err.reason);
                return;
            }
            stopWorking(err);
            if(response){
                $(".addServiceConfigHandle .ui.message").removeClass("warning success").addClass("error");
                addServiceConfigSidebar.sidebar("toggle");
            }
        });
        return false
    };
    onFailureCallback= function() {
        $(".addServiceConfigHandle .ui.message").removeClass("warning success").addClass("error");
        return false;
    };
    formOptions= {
        inline: true,
        on: 'blur',
        onSuccess: onSuccessCallback,
        onFailure: onFailureCallback
    };

    $('.addServiceConfigForm').form(_.extend({fields:formRules},formOptions));

};

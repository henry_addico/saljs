import dragular from 'dragula/dragula';
import {getRequiredDate} from '../../lib/slotHelpers';
import {setRequiredDate} from '../../lib/slotHelpers';
import {Booking, undoBooking} from '../../lib/slotHelpers';

import 'dragula/dist/dragula.css'
Template.slotsMenu.helpers({
    hasPayment:function(appointmentId) {
        var appointment  = Appointments.findOne({_id:appointmentId});
        if(appointment){
            return appointment.payments !=null &&  appointment.payments.length!=0
        }
        return false;
    },
});
Template.slots.helpers({
    dayString: dayStringHelper,
    dateString: dateStringHelper,
    showPreviousWidget: function () {
        var sessionDate = new Date(getRequiredDate());
        var currentDate = new Date();
        return canAdjustMonth(-1, sessionDate, currentDate);
    },
    getDays: getDaysHelper,
    slotsForDate: slotsForDateHelper,
    slotIsBooked: function(givenSlot){
        return givenSlot.state == Booking.STATE_BOOKED;
    },
    slotIsReserved:function(givenSlot){
        return givenSlot.state == Booking.STATE_RESERVED;
    },
    totalNumberReserved: function(){
        var slotHelper = slotsForDateHelper();
        var totalNumberOfReserved = 0;
        if(slotHelper && slotHelper.slots) {
            var slots = slotHelper.slots;
            for (var index = 0; index < slots.length; index++) {
                if (slots[index].state == Booking.STATE_RESERVED) {
                    totalNumberOfReserved++;
                }
            }
        }
        return totalNumberOfReserved;
    },
    totalNumberBooked: function(){
        var slotHelper = slotsForDateHelper();
        var totalNumberOfBooked = 0;
        if(slotHelper && slotHelper.slots) {
            var slots = slotHelper.slots;
            for (var index = 0; index < slots.length; index++) {
                if (slots[index].state == Booking.STATE_BOOKED) {
                    totalNumberOfBooked++;
                }
            }
        }
        return totalNumberOfBooked;
    },
    hasPayment:function(appointmentId) {
        var appointment  = Appointments.findOne({_id:appointmentId});
        if(appointment){
            return appointment.payments !=null &&  appointment.payments.length!=0
        }
        return false;
    },
});


Template.slots.events = {
    'click .addSlotWidget': addSlot,
    'click .extendTopHandle': function(){
        addSlot(undefined,true,false);
    },
    'click .extendBottomHandle': function(){
        addSlot(undefined,true,true);
    },
    'click .clearSlotWidget': function(){
        undoBooking(this);
    },
    'click .addPaymentHandle': function(){
        var appointment = Appointments.findOne({_id:this.appointment});
        Session.set("appointment", appointment);
        //setValue('serviceDetails', appointment.serviceDetails);
    },
    'click .toggleBlockingSlotWidget': toggleBlockingOfSlot,
    'click .RowText': function () {
        Session.set("Editing", this._id);
    },
    'click .Close': function () {
        slots.remove({'_id': this._id});
    },
    'click .nextWidget': function () {
        adjustMonth(1);
    },
    'click .previousWidget': function () {
        adjustMonth(-1);
    },
    'click .dayWidget': function () {
        var sessionDate = new Date(getRequiredDate());
        sessionDate.setDate(this.number);
        setRequiredDate(sessionDate);
    }
};

Template.addSlotsSidebar.helpers({
    serviceConfig: function(){
        return ServiceConfig.find().fetch();
    },
    requiredDateFormated:function(){
        return  moment(getRequiredDate(),DateFormats.defjs).format(DateFormats.ddmmyyyy)
    },
    opad: function(value){
        if(value < 10)
            return "0"+value;
        return value;
    }

})
Template.slotsMenu.onRendered(function () {
    
  });

Template.slots.rendered= function(){
    Template.addSlotsSidebar.rendered();
    Template.calendarSidebar.rendered();
    Template.reserveDetailsSidebar.rendered();
    Template.paymentSidebar.rendered();
    const drake = dragular([$('#container').get(0)], { 
        isContainer: function (el) {
        return false; // only elements in drake.containers will be taken into account
      },
      moves: function (el, source, handle, sibling) {
        return true; // elements are always draggable by default
      },
      accepts: function (el, target, source, sibling) {
        var slotHelper = slotsForDateHelper();
        console.log(slotHelper.slots[el.rowIndex-1]);
        return slotHelper.slots[el.rowIndex-1].state == Booking.STATE_FREE; // elements can be dropped in any of the `containers` by default
      },
      invalid: function (el, handle) {
        return false; // don't prevent any drags from initiating by default
      },
      direction: 'vertical',             // Y axis is considered when determining where an element would be dropped
      copy: false,                       // elements are moved by default, not copied
      copySortSource: false,             // elements in copy-source containers can be reordered
      revertOnSpill: false,              // spilling will put the element back where it was dragged from, if this is true
      removeOnSpill: false,              // spilling will `.remove` the element, if this is true
      mirrorContainer: document.body,    // set the element that gets mirror elements appended
      ignoreInputTextSelection: true     // allows users to select input text, see details below
    }).on('drag', function (el) {
        console.log('drag');
        //el.className = el.className.replace('ex-moved', '');
      })
      .on('drop', function (el) {
        console.log('drop');
        //el.className += ' ex-moved';
      })
      .on('over', function (el, container) {
        console.log('over');
        //container.className += ' ex-over';
      })
      .on('out', function (el, container) {
        console.log('out');
        //container.className = container.className.replace('ex-over', '');
      });;
}

Template.addSlotsSidebar.rendered = function(){
    addSlotsSidebar = $('.addSlotsSidebar').first().sidebar('setting', 'transition', 'overlay');
    if($(".addSlotsSidebar").length) {
        addSlotsSidebar.sidebar('attach events', '.addSlotsHandle');
    }
    if (!Modernizr.inputtypes.date) {
        $('.datepicker').pickadate({container: '#root-picker-outlet',format: PickerFormats.ddmmyyyy});
    }
    $.fn.form.settings.rules.toDateAfterFromDate = function(value) {
        fromDate = parseDate($('input[name="fromDate"]').val());
        toDate = parseDate(value);
        return fromDate.isSame(toDate) || fromDate.isBefore(toDate);
    }
    formRules = {
        fromDate:{
            identifier  : 'fromDate',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please choose a day to start adding slot'
                }
            ]},
        toDate:{
            identifier  : 'toDate',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please choose the last day'
                },
                {
                    type: 'toDateAfterFromDate',
                    prompt : 'The end Date needs to same day or after the from date'
                }
            ]}
    };
    onSuccessCallback = function() {
        console.log("on success");
        addSlotsSidebar.sidebar("toggle");
        $(".addSlotsHandle .ui.message").removeClass("warning error").addClass("success");
        fromDate = parseDate($('input[name="fromDate"]').val());
        toDate   = parseDate($('input[name="toDate"]').val());
        do{
            addSlot(fromDate.toDate().toDateString(),false);
            fromDate.add(1,'days');
        }while(fromDate.isSame(toDate) || fromDate.isBefore(toDate));
        return false
    };
    onFailureCallback= function() {
        console.log("on failure");
        $(".addSlotsHandle .ui.message").removeClass("warning success").addClass("error");
        return false;
    };
    formOptions= {
        inline: true,
        on: 'blur',
        onSuccess: onSuccessCallback,
        onFailure: onFailureCallback
    };

    $('.addSlotsForm').form(_.extend({fields:formRules},formOptions));

};
Template.paymentSidebar.rendered = function(){
    paymentSidebar = $('.paymentSidebar').first().sidebar('setting', 'transition', 'overlay');
    if($(".paymentSidebar").length) {
        paymentSidebar.sidebar('attach events', '.addPaymentHandle');
    }
    formRules = {
        actualPrice:{
            identifier  : 'actualPrice',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please specify an amount'
                },
                {
                    type: 'decimal',
                    prompt : 'Please specify a valid amount'
                }
            ]}
    };
    onSuccessCallback = function() {
        console.log("on success");
        paymentSidebar.sidebar("toggle");
        $(".addPaymentHandle .ui.message").removeClass("warning error").addClass("success");
        const actualPrice = Number($('input[name="actualPrice"]').val());
        const appointment  = Session.get('appointment');
        Meteor.call('postInvoice', appointment._id, actualPrice, function(err,response){
            if(err) {
                Session.set('errorMessage',err.message);
            }else{

            }
        });
        return false
    };
    onFailureCallback= function() {
        console.log("on failure");
        $(".addPaymentHandle .ui.message").removeClass("warning success").addClass("error");
        return false;
    };
    formOptions= {
        inline: true,
        on: 'blur',
        onSuccess: onSuccessCallback,
        onFailure: onFailureCallback
    };

    $('.addPaymentForm').form(_.extend({fields:formRules},formOptions));

};

Template.paymentSidebar.helpers({
    paymentTypes: function(){
        return [{name:'cash', description:'cash'}, 
        {name:'card', description:'card'}, 
        {name:'cheque', description:'cheque'}];
    },
    totalPrice: function () {
        var appointment = Session.get('appointment');
        if (!appointment){
            return 0;
        }
        return _.reduce(_.map(appointment.serviceDetails,priceMapper),sumReducer,0);
    },
    currentAppointment: function () {
        var appointment = Session.get('appointment');
        if (!appointment){
            appointment = {priceFrom:0, actualPrice:0}
        }
        return appointment;
    }
});
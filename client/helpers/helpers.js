import {getRequiredDate} from '../../lib/slotHelpers';
import {setRequiredDate} from '../../lib/slotHelpers';
import {Booking} from '../../lib/slotHelpers';

this.clearMessages = function(){
    Session.set("successMessage",undefined);
    Session.set("errorMessage",undefined);
    Session.set("alertMessage",undefined);
    Session.set("warningMessage",undefined);
    Session.set("dialogErrorMessage",undefined);

}
this.setValue = function(property,value){
    var givenModel = model();
    if(!givenModel){
        createDefaultModel();
        givenModel = model();
    }

    givenModel[property]=value;
    return model(givenModel);
}
this.createDefaultModel = function(){
    defaultModel = {
        slotRange:'.nextWeek'
    };
    if(Meteor.userId()){
        defaultModel = {
            contactNumber: Meteor.user().profile.contactNumber,
            contactName: Meteor.user().profile.contactName,
            emailAddress: Meteor.user().emails[0].address,
            slotRange:'.nextWeek'
        };
    }
    model(defaultModel);
    var sessionDate = moment().add(7, 'days');
    setRequiredDate(sessionDate.toDate());

}
this.getValue = function(property){
    var givenModel = model();
    if(givenModel){
        return givenModel[property];
    }
    return undefined;
}
this.startWorking = function(){
    Session.set('working', true);
    $(".btn").attr('disabled','disabled');
    clearMessages();
    NProgress.start();
}
this.currentDate = function(date){
    if (date){
        Session.set("Date", date.toDateString());
        return date;
    }else{
        return Session.get("Date")? new Date(Session.get("Date")): null;
    }
}
this.stopWorking = function(){
    Session.set('working', false);
    $(".btn").removeAttr('disabled');
    Session.set('working', false);
    NProgress.done();
}
this.model = function(model){
    if(model){
        Session.set("appointmentModel",model);
        return model;
    }
    //if(!Session.get("appointmentModel")){
    //Session.set("appointmentModel", Meteor.userId() ? {'userId': Meteor.userId()}: {});
    //}
    return Session.get("appointmentModel");
}

UI.registerHelper('user', function (input) {
    return (Meteor.user() == null) ? {} : {
        firstName: "Henry",
        lastName: "Addico",
        emailAddress: Meteor.user().emails[0].address,
        contactNumber: "07738370760"
    };
});
UI.registerHelper('model', function () {
    return model();
});

UI.registerHelper('requiredDate', function () {
    return getRequiredDate();
});

UI.registerHelper("message", function (key) {
    return Session.get(key)
});
this.DateFormats = {
    short: "DD MMMM - YYYY",
    long: "dddd DD.MM.YYYY",
    defjs: "ddd MMM DD YYYY",
    ddmmyyyy: "DD/MM/YYYY",
    ddmmyyyydash: "DD-MM-YYYY",
    yyyymmdddash: "YYYY-MM-DD",
    ddmmmyyyyspaces: "DD MMM YYYY"
};
this.PickerFormats = {
    ddmmyyyy: "dd/mm/yyyy"
};
this.parseDate = function(value){
    return moment(value,[DateFormats.ddmmyyyy,DateFormats.ddmmmyyyyspaces,DateFormats.yyyymmdddash,DateFormats.ddmmyyyydash,DateFormats.short,DateFormats.long,DateFormats.defjs]);
};

UI.registerHelper('working', function () {
    return Session.get('working')
});
UI.registerHelper("formatDate", function (datetime, format) {
    if (moment) {
        f = DateFormats[format];
        return f ? moment(datetime,DateFormats.defjs).format(f) : moment(datetime,DateFormats.defjs).format(format);
    }
    else {
        return datetime;
    }
});

UI.registerHelper('contentURL', function(){
    return Meteor.settings.public.contentUrl;
});

UI.registerHelper('totalPrice', function(){
    serviceDetails = getValue("serviceDetails");
    return _.reduce(_.map(serviceDetails,priceMapper),sumReducer,0);
});
UI.registerHelper('totalDuration', function(){
    serviceDetails = getValue("serviceDetails");
    return _.reduce(_.map(serviceDetails,durationMapper),sumReducer,0);
});

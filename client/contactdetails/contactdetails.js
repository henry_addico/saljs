Template.contactDetailsSidebar.helpers(
    {
        contactNumberEntered : function(){
            return model().contactNumber ? false : true;
        }
    }
);

Template.contactDetailsSidebar.rendered = function(){
    contactDetailsSidebar = $('.contactDetailsSidebar').first().sidebar('setting', 'transition', 'overlay');
    if($(".contactDetailshandle").length) {
        contactDetailsSidebar.sidebar('attach events', '.contactDetailshandle');
    }
    $.fn.form.settings.rules.contactNumber = function(value) {
        urlRegExp = /^[\+]{0,1}[0-9]{7,14}$/;
        return urlRegExp.test(value)
    }
    formRules = {
        contactNumber:{
            identifier  : 'contactNumber',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please enter a valid telephone or mobile number'
                },
                {
                    type: 'contactNumber',
                    prompt : 'min of 7 characters (+ and only numbers are allowed)'
                }
            ]},
        contactName:{
            identifier  : 'contactName',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please provide your full name'
                }
            ]},
        emailAddress:{
            identifier  : 'emailAddress',
            optional   : true,
            rules: [
                {
                    type   : 'email',
                    prompt : 'Please enter a valid email address'
                }
            ]}
    };
    onSuccessCallback = function() {
        console.log("on success");
        contactDetailsSidebar.sidebar("toggle");
        $(".contactDetailshandle .ui.message").removeClass("warning error").addClass("success");
        return false
    };
    onFailureCallback= function() {
        console.log("on failure");
        $(".contactDetailshandle .ui.message").removeClass("warning success").addClass("error");
        return false;
    };
    formOptions= {
        inline: true,
        on: 'blur',
        onSuccess: onSuccessCallback,
        onFailure: onFailureCallback
    };

    $('.contactDetailsForm').form(_.extend({fields:formRules},formOptions));
}

Template.contactDetailsSidebar.events = {
    "blur":function(event){
        setValue(event.target.name,event.target.value);
    },
    "keypress .contactNumber": function(event){
        if(event.target.value.length > 5){
            $(".ui.accordion").accordion('open',1);
        }
    }
};

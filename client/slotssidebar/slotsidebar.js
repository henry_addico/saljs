import {getRequiredDate} from '../../lib/slotHelpers';
import {setRequiredDate} from '../../lib/slotHelpers';
import {Booking} from '../../lib/slotHelpers';

function slotChange (daysClass,daysOffset){
    //if(!$(daysClass).hasClass('active')){
        //$("ul.slotsFilter > li").removeClass('active');
        //$(daysClass).parent().addClass('active');
        var sessionDate = moment().add(daysOffset, 'days');
        //setRequiredDate(sessionDate.toDate());
        setValue("slotRange",daysClass);
    //}
}
function selectSlotWidget(event){
    event.preventDefault();
    //Session.set("selectedSlot",this);
    setValue("selectedSlot",this);
    appointmentModel = model();
    appointmentModel.slotId=this.id;
    appointmentModel.slotIndex=this.index;
    appointmentModel.slotDate=this.date;
    appointmentModel.slotStart=this.start.hour+(this.start.minute>10 ? ":": ":0")+this.start.minute;
    appointmentModel.slotEnd=this.end.hour+(this.end.minute>10 ? ":": ":0")+this.end.minute;
    model(appointmentModel);
}


Template.slotsSidebar.rendered = function(){
    slotsSidebar = $('.slotsSidebar').first().sidebar('setting', 'transition', 'overlay');
    if($(".slotsHandle").length) {
        slotsSidebar.sidebar('attach events', '.slotsHandle');
    }
    formRules = {
        selectedSlot:{
            identifier  : 'selectedSlot',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please select a slot'
                }
            ]}
    };
    onSuccessCallback = function() {
        console.log("on success");
        slotsSidebar.sidebar("toggle");
        $(".slotsHandle .ui.message").removeClass("warning error").addClass("success");
        return false
    };
    onFailureCallback = function() {
        console.log("on failure");
        $(".slotsHandle .ui.message").removeClass("warning success").addClass("error");
        return false;
    };
    formOptions = {
        inline: false,
        on: 'blur',
        onSuccess: onSuccessCallback,
        onFailure: onFailureCallback
    };

    $('.slotsForm').form(_.extend({fields:formRules},formOptions));
}

Template.slotsSidebar.events = {
    'click .selectSlotWidget': selectSlotWidget,
    'click .nextWidget': function () {
        adjustMonth(1);
    },
    'click .previousWidget': function () {
        adjustMonth(-1);
    },
    'click .nextWeek': function () {
        slotChange('.nextWeek', 7)
    },
    'click .nextday': function () {
        slotChange('.nextday', 2)
    },
    'click .in2Week': function () {
        slotChange('.in2Week', 14)
    },
    'click .in1Month': function () {
        slotChange('.in1Month', 28)
    },
    'click .in2Months': function () {
        slotChange('.in2Months', 356)
    },
    'click .dayWidget': function () {
        var sessionDate = new Date(getRequiredDate());
        sessionDate.setDate(this.number);
        setRequiredDate(sessionDate);
    }
};
Template.slotsSidebar.helpers({
    dayString: dayStringHelper,
    dateString: dateStringHelper,
    getDays: getDaysHelper,
    groupedSlotsForDate: function(){
        var slotRange = getValue("slotRange");
        var daysOffset =0;
        if(slotRange==".nextWeek"){
            daysOffset = 7;
        } else if(slotRange==".nextday"){
            daysOffset = 2;
        } else if(slotRange==".in2Week"){
            daysOffset = 14;
        } else if(slotRange==".in1Month"){
            daysOffset = 28;
        } else if(slotRange==".in2Months") {
            daysOffset = 365;
        }else{
            console.log("unknown daysoffset");
        }
        var sessionDate = moment().add(daysOffset, 'days');
        return  groupedSlotsForDate(sessionDate.toDate());
    },
    selectedSlot: function (slot) {

        var currentModel = model();
        return currentModel && currentModel.selectedSlot && currentModel.selectedSlot.index == slot.index && currentModel.selectedSlot.date == slot.date ? "selected-service" : "";
    },
    hasSelectedSlot: function () {
        var currentModel = model();
        return currentModel && currentModel.selectedSlot ? true : false;
    },
    activeClass: function (slotClass) {
        var currentModel = model();
        return currentModel && slotClass == model().slotRange ? "active" : "";
    },
    showSlot: function () {
        return (this.state == Booking.STATE_FREE || this.state == Booking.STATE_RESERVED);
    },
    isNumber: function () {
        return !isNaN(this.number);
    }
});

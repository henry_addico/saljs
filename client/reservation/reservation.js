import {getRequiredDate, setRequiredDate, Booking} from '../../lib/slotHelpers';

Template.reserveDetailsSidebar.rendered = function(){
    reserveDetailsSidebar = $('.reserveDetailsSidebar').first().sidebar('setting', 'transition', 'overlay');
    if($(".reservationHandle").length) {
        reserveDetailsSidebar.sidebar('attach events', '.reservationHandle');
    }

    formRules = {
        description:{
            identifier  : 'description',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please enter a description'
                }
            ]},
        durationInMinutes:{
            identifier  : 'durationInMinutes',
            rules: [
                {
                    type   : 'integer',
                    prompt : 'Please enter duration in minutes'
                }
            ]},
        location:{
            identifier  : 'location',
            optional   : true,
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Please enter a location'
                }
            ]}
    };
    onSuccessCallback = function() {
        clearMessages();
        startWorking();
        var appointmentModel = model();
        Meteor.call('reserveSlot', appointmentModel, function(err,response) {
            console.log(err);
            if(err) {
                Session.set('dialogErrorMessage', err.message);
            }else{
                appointmentModel._id = response;
                model(appointmentModel);
                reserveDetailsSidebar.sidebar("toggle");
                $(".reservationHandle .ui.message").removeClass("warning error").addClass("success");
            }
            stopWorking();
        });
        return false
    };
    onFailureCallback= function() {
        $(".reservationHandle .ui.message").removeClass("warning success").addClass("negative");
        return false;
    };
    formOptions= {
        inline: true,
        on: 'blur',
        onSuccess: onSuccessCallback,
        onFailure: onFailureCallback
    };

    $('.reserveDetailsForm').form(_.extend({fields:formRules},formOptions));
}

function selectSlotWidget(event){
    event.preventDefault();
    setValue("selectedSlot",this);
    appointmentModel = model();
    appointmentModel.slotId=this.id;
    appointmentModel.slotIndex=this.index;
    appointmentModel.slotDate=this.date;
    appointmentModel.slotStart=this.start.hour+(this.start.minute>10 ? ":": ":0")+this.start.minute;
    appointmentModel.slotEnd=this.end.hour+(this.end.minute>10 ? ":": ":0")+this.end.minute;
    model(appointmentModel);
}

Template.reserveDetailsSidebar.events = {
    "blur input":function(event){
        setValue(event.target.name,event.target.value);
    },
    "change .durationInMinutes":function(event){
        setValue(event.target.name,event.target.value);
    },
    'click .selectSlotWidget': selectSlotWidget
};


this.groupedSlotsForDay = function () {
    slotsHandle = {slots: []};
    endDate = new Date(getRequiredDate());
    requiredDuration = Number(getValue("durationInMinutes"));
    var cdate = moment().hour(0).minutes(0).seconds(0);
    if(endDate) {
        requiredDates = [];
        requiredDates.push(endDate.toDateString());
        serviceSlotCursor = ServiceSlots.find({'date': {$in: requiredDates}});

        serviceSlotCursor.forEach(function (serviceSlot) {
            minimumSlotDuration = requiredDuration;
            serviceSlot.date=endDate.toDateString();
            if (minimumSlotDuration && serviceSlot) {
                slotsHandle.slots = groupSlots(serviceSlot, minimumSlotDuration, slotsHandle.slots);
            }
        });
        slotsHandle.slots = _.sortBy(slotsHandle.slots, function (givenSlot) {
            dd = moment(new Date(givenSlot.date));
            return dd.toDate().getTime();
        });
    }
    return slotsHandle;
}
Template.reserveDetailsSidebar.helpers({
    groupedSlotsForDate: groupedSlotsForDay,
    selectedSlot: function (slot) {
        var currentModel = model();
        return currentModel && currentModel.selectedSlot && currentModel.selectedSlot.index == slot.index && currentModel.selectedSlot.date == slot.date ? "selected-service" : "";
    },
    hasSelectedSlot: function () {
        var currentModel = model();
        return currentModel && currentModel.selectedSlot ? true : false;
    },
    activeClass: function (slotClass) {
        var currentModel = model();
        return currentModel && slotClass == model().slotRange ? "active" : "";
    },
    showSlot: function () {
        return (this.state == Booking.STATE_FREE || this.state == Booking.STATE_RESERVED);
    },
    isNumber: function () {
        return !isNaN(this.number);
    }
});

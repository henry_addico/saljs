import {getRequiredDate} from '../../lib/slotHelpers';
import {setRequiredDate} from '../../lib/slotHelpers';
import {Booking} from '../../lib/slotHelpers';

if ((Session.get(Booking.NAVIGATION_KEY) == null)) {
  Session.set(Booking.NAVIGATION_KEY, new Date().toDateString());
}

Template.calendarSidebar.helpers({
   navigationDateData(){
       return {
         sessionKey: Booking.NAVIGATION_KEY,
         size: "small"
       };
     },
   dateValue(context){ return Session.get(context.sessionKey); }
});

Template.calendarSidebar.rendered = function(){
  let calendarSidebar = $('.calendarSidebar').first().sidebar('setting', 'transition', 'overlay');
  if ($(".dateHandle").length) {
    calendarSidebar.sidebar('attach events', '.dateHandle');
  }
  let formRules = {
    dateSelection:{
      identifier  : 'dateSelection',
      rules: [
        {
          type   : 'empty',
          prompt : 'Please select a date'
        }
      ]}
  };

  let onSuccessCallback = function(){
    console.log("on success");
    calendarSidebar.sidebar("toggle");
    $(".dateHandle .ui.message").removeClass("warning error").addClass("success");
    return false;
  };

  let onFailureCallback= function(){
    console.log("on failure");
    $(".dateHandle .ui.message").removeClass("warning success").addClass("error");
    return false;
  };

  let formOptions= {
    inline: true,
    on: 'blur',
    onSuccess: onSuccessCallback,
    onFailure: onFailureCallback
  };

  return $('.calendarForm').form(_.extend({fields:formRules},formOptions));
};

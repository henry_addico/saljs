/**
 * Created by Sly on 19/06/15.
 */
Template.messages.rendered = function() {
    $(".nav-button").click(function(){
        return $("nav").slideToggle(),!1
    });
}

Template.footer.helpers({
  year: function() {
    return moment().format('YYYY');
  }
});

import {Booking} from '../../lib/slotHelpers';


export let canAdjustMonth = function(adjustMonthBy,sessionDate,currentDate){
  let change=false;
  if (Roles.userIsInRole(Meteor.userId(), ['admin'])) {
    change=true;
  } else if ((adjustMonthBy>0) ||  (sessionDate.getMonth()> currentDate.getMonth()) || (sessionDate.getFullYear() > currentDate.getFullYear())) {
    change=true;
  }
  return change;
}

export let adjustMonth = function(adjustMonthBy,sessionKey) {
  let sessionDate = new Date(Session.get(sessionKey));
  let currentDate = new Date();
  let change=canAdjustMonth(adjustMonthBy,sessionDate,currentDate);
  if (change) {
    if (sessionDate.getDate() > 28) {
      sessionDate.setDate(28);
    }
    sessionDate.setMonth(sessionDate.getMonth()+adjustMonthBy);
    return Session.set(sessionKey, sessionDate.toDateString());
  }
}

export let adjustTime = function(adjustMonthBy,sessionKey) {
  let time = Session.get(sessionKey);
  let change=false;
  if (adjustMonthBy > 0) {
    if (time < (24*60)) {
      change=true;
    }
  } else {
    if (time > 0) {
      change=true;
    }
  }
  if (change) {
    time+=adjustMonthBy;
  }
  return Session.set(sessionKey, time);
}

export let amOrPm = function(sessionKey){
  let hour = Math.floor(Session.get(sessionKey)/60);
  if (hour >= 12) {
    return "PM";
  } else {
    return "AM";
  }
}

Template.datePicker.helpers({
  showPreviousWidget(){
    let sessionDate = new Date(Session.get(this.sessionKey));
    let currentDate = new Date();
    return canAdjustMonth(-1,sessionDate,currentDate);
  },

  dateString(){
    let D = new Date(Session.get(this.sessionKey));
    return Booking.Months[D.getMonth()] + ", " + D.getFullYear();
  },

  getDays(){
    let i;
    let sessionDate = new Date(Session.get(this.sessionKey));
    let monthDate = moment(sessionDate).date(1);
    let nextMonthDate = moment(sessionDate).date(1).add(1, 'months').subtract(1,'days');
    let days = [
      {number: "Su", text:"Su"},
      {number: "Mo",text: "Mo"},
      {number: "Tu",text:"Tu"},
      {number: "We",text:"We"},
      {number: "Th",text:"Th"},
      {number: "Fr", text:"Fr"},
      {number: "Sa", text: "Sa"}
    ];
    if (monthDate.day() > 0) {
      for (i = 0, end = monthDate.day()-1, asc = 0 <= end; asc ? i <= end : i >= end; asc ? i++ : i--) {
        var asc, end;
        days.push({text : "&nbsp;&nbsp;&nbsp;&nbsp;", number:i-monthDate.day() });
      }
    }
    for (i = 1, end1 = nextMonthDate.date(), asc1 = 1 <= end1; asc1 ? i <= end1 : i >= end1; asc1 ? i++ : i--) {
      var asc1, end1;
      if (sessionDate.getDate() === i) {
        if (i< 10) {
          days.push({'number' : i,'text' :  `&nbsp;&nbsp;${i}`, 'activeClass': " active"});
        } else {
          days.push({'number' : i,'text' : i, 'activeClass': " active"});
        }
      } else {
        if (i< 10) {
          days.push({'number' : i,'text' : `&nbsp;&nbsp;${i}`, 'activeClass': ""});
        } else {
          days.push({'number' : i,'text' :i, 'activeClass': ""});
        }
      }
    }
    let additionalPad = days.length % 7;
    if (additionalPad !== 0) {
      for (i = 0, end2 = 6-additionalPad, asc2 = 0 <= end2; asc2 ? i <= end2 : i >= end2; asc2 ? i++ : i--) {
        var asc2, end2;
        days.push({text : "&nbsp;&nbsp;&nbsp;&nbsp;", number:i+nextMonthDate.day()+1 });
      }
    }
    return days;
  }
});

Template.datePicker.events = {
  'click .nextWidget'(){ return adjustMonth(1,this.sessionKey); },
  'click .previousWidget'(){ return adjustMonth(-1,this.sessionKey); },
  'click .dayWidget'(originalEvent,context){
    let sessionDate = new Date(Session.get(context.data.sessionKey));
    sessionDate.setDate(this.number);
    return Session.set(context.data.sessionKey, sessionDate.toDateString());
  }
};
Template.timePicker.events = {
  'click .nextWidget'(){ return adjustTime(30,this.sessionKey); },
  'click .previousWidget'(){ return adjustTime(-30,this.sessionKey); }
};

Template.timePicker.helpers({
  amOrPm(){ return amOrPm(this.sessionKey); },
  hour(context){
    let hour = Math.floor(Session.get(this.sessionKey)/60);
    if (hour < 10) {
      return `0${hour}`;
    } else {
      return hour;
    }
  },
  min(context){
    if ((Session.get(this.sessionKey)%60) === 0) {
      return "00";
    } else {
      return "30";
    }
  }
});

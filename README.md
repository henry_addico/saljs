# Saljs

Please install meteor from [meteor.com](www.meteor.com)
clone this git repo some where and then run the following commands at the top level directory

```
#!shell

meteor npm install
```

To run the app

```
#!shell

source ./.deploy/dev/env.sh && meteor  run --settings .deploy/dev/settings.json

# devtest remote mongo db
. ./.deploy/devtest/env.sh && meteor  run --settings .deploy/devtest/settings.json

```

Connect to mongodb

```
#!shell

meteor mongo
```

Server-side debugging

```
#!shell

source ./config/dev/env.sh && meteor debug --settings config/dev/settings.json

```
Connect to the debugger using browser, debugger will pause on the serverside code were 'debugger' term has been added

http://localhost:8080/debug?port=5959

Backup

http://meteor-up.com/docs.html#backup-and-restore

Deployment
You will need to be in the root directory to use terraform to deploy. It is advised that you test the app on app.roonechase.com or app2.rooneychase.com depending on which is not in used. Check the dns record for my.rooneychase.com to be sure. Update compute.tf to change the deault var from 1/2 depending on which one you want to deploy

```
#!shell
meteor npm run tfgreenp
#if its all ok, run the command below you will be prompted to type yes or no to continue
meteor npm run tfgreena
#if upu need to deploy to app2 blue 
meteor npm run tfbluep
meteor npm run tfbluea
```

depending on which dns you choose cd into the directory e.g digitalocean for app or digitalocean2 or app2.

```
 mup setup

mup deploy
#make sure there are no cert registration errors
mup proxy logs-le
```

Backup restore data

Make sure ip in runbackup ang runimport are pointing to the right ips and then run
Note that you need to be in either digitalocean or digitalocean2 depending on which was was used to deploy the app. 

currently using digitalocean
```
 bash ./runbackup.sh 

 bash ./runimport.sh 

#  Cloud import
 bash ./runimportcloud.sh 

# need mongo db install to import to the cloud 
 brew install mongodb-community@4.4

ssh -t -o 'UserKnownHostsFile=/dev/null' -o 'StrictHostKeyChecking=no' root@app2.rooneychase.com "/usr/bin/mongoexport -h localhost:27017 -d saljs -c appointments -o backup/appointments.json"
 ```

 If you are happy you can point my.rooneychase to the new dns, update mup.js to point to my.rooneychase alone and then run 

 ```
mup reconfig
#make sure there are no cert registration errors
mup proxy logs-le
 ```

Avoid destoying the VM


```
terraform untaint digitalocean_droplet.rooneychase-com-www1
```

Destroy an instance

```
meteor npm run tfdd
```


## ssh fails or mup ssh fails
ssh-add 

## Adding graphQL support 
npm install --save graphql
npm install --save graphql-client
npm install --save graphql-tag

npm install --save apollo-cache-inmemory
npm install --save apollo-client
npm install --save apollo-link-http
npm install --save node-fetch
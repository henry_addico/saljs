this.fetchAppointment = function(){
    var appointmentId= Router.current().params.appointmentId;
    startWorking();
    Meteor.call('getAppointment', appointmentId, function(err,response) {
        if(err) {
            console.log(err);
            Session.set('serverDataResponse', "Error:" + err.reason);
            stopWorking(err);
            return;
        }
        stopWorking(err);
        model(response);
    });
    return true;
}


Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'notFound'
});

Router.map(function () {
    this.route('book', {
        path: '/book',
        template: 'book',
        waitOn: function(){return [subs.subscribe("serviceGroups"),subs.subscribe('serviceTypes'),subs.subscribe('serviceConfig'),subs.subscribe("serviceSlots")];},
        layoutTemplate: "bookinglayout",
        onBeforeAction: function(){
            Session.set("appointmentModel",void 0);
            this.next();
        }
    });
    this.route('home', {
        path: '/',
        template() {
          if (Meteor.userId() != null && Roles.userIsInRole(Meteor.userId(), ['admin'])) {
            return 'slots';
          } else {
            return 'book';
          }
        },
        layoutTemplate: "bookinglayout",
        waitOn: function(){return [subs.subscribe("serviceGroups"),subs.subscribe('serviceTypes'),subs.subscribe('serviceConfig'),subs.subscribe("serviceSlots"),subs.subscribe("appointments")];},
        onBeforeAction: function(){
            Session.set("appointmentModel",void 0);
            Session.set("selected_serviceType",void 0);
            Session.set("selected_serviceDetail",void 0);
            this.next();
        },
        onAfterAction: function () {

        },
    });
    this.route('entrySignOut', {
        path: '/sign-out',
        onBeforeAction: function(){
            Meteor.logout ();
            Router.go('home');
            this.next();
        }
    });
    this.route('dashboard', {
        path: '/dashboard',
        template: 'dashboard'
    });
    this.route('contact', {
        path: '/contact/:contactId',
        template: 'contact'
    });
    this.route('slot', {
        path: '/slot',
        waitOn: function(){return [subs.subscribe("serviceGroups"),subs.subscribe('serviceTypes'),subs.subscribe('serviceConfig'),subs.subscribe("serviceSlots"),subs.subscribe("appointments")];},
        layoutTemplate: "bookinglayout",
        template: 'slots'
    });
    this.route('services', {
        path: '/services',
        waitOn: function(){return [subs.subscribe("serviceGroups"),subs.subscribe('serviceTypes'),subs.subscribe('serviceConfig'),subs.subscribe("serviceSlots")];},
        layoutTemplate: "servicesLayout",
        template: 'services'
    });
    this.route('servicesGroups', {
        path: '/services/groups',
        waitOn: function(){return [subs.subscribe("serviceGroups"),subs.subscribe('serviceTypes'),subs.subscribe('serviceConfig'),subs.subscribe("serviceSlots")];},
        layoutTemplate: "servicesLayout",
        template: 'serviceGroups'
    });
    this.route('serviceConfigs', {
        path: '/services/config',
        waitOn: function(){return [subs.subscribe("serviceGroups"),subs.subscribe('serviceTypes'),subs.subscribe('serviceConfig'),subs.subscribe("serviceSlots")];},
        layoutTemplate: "servicesLayout",
        template: 'serviceConfigs'
    });
    this.route('thanks', {
        path: '/thanks/:appointmentId',
        waitOn: function(){return [function(){return fetchAppointment();}]},
        template: 'thanksStep'
    });
    this.route('appointment', {
        path: '/appointment/:appointmentId',
        waitOn: function(){return [subs.subscribe("serviceGroups"),subs.subscribe('serviceTypes'),subs.subscribe('serviceConfig'),subs.subscribe("serviceSlots"),function(){return fetchAppointment();}]},
        template: 'book',
        layoutTemplate: "bookinglayout"
    });

    this.route('admin', {
        path: '/admin',
        template: 'adminusers'
    });
});

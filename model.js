// All ServiceTypes for a  booking application
// Loaded on both the client and the server

///////////////////////////////////////////////////////////////////////////////
// ServiceTypes and ServiceSlots

/*
  Each ServiceType is represented by a document in the ServiceType collection:
*/
ServiceTypes = new Meteor.Collection("serviceTypes");
ServiceGroups = new Meteor.Collection("serviceGroups");
ServiceSlots = new Meteor.Collection("serviceSlots");
ServiceConfig = new Meteor.Collection("serviceConfig");
Appointments = new Meteor.Collection("appointments");
Contacts = new Meteor.Collection("contacts");


function adminUser(userId) {
  return Roles.userIsInRole(userId, ['admin']);
}
Contacts.allow({
  insert: function(userId, doc){
   return true;
 },
 update: function(userId, docs, fields, modifier){
   return true;
 }
});
Appointments.allow({
   insert: function(userId, doc){
    return true;
  },
  update: function(userId, docs, fields, modifier){
    return true;
  }
});
ServiceTypes.allow({
   insert: function(userId, doc){
    return adminUser(userId);
  },
  update: function(userId, docs, fields, modifier){
    return adminUser(userId);
  },
  remove: function (userId, docs){
    return adminUser(userId) ;
  }
});
ServiceGroups.allow({
   insert: function(userId, doc){
    return adminUser(userId);
  },
  update: function(userId, docs, fields, modifier){
    return adminUser(userId);
  },
  remove: function (userId, docs){
    return adminUser(userId) ;
  }
});
ServiceConfig.allow({
  update: function(userId, docs, fields, modifier){
    return adminUser(userId);
  }
});
ServiceSlots.allow({
  insert: function(userId, doc){
    return adminUser(userId);
  },
  update: function(userId, docs, fields, modifier){
    return (fields.length==1 && fields[0]=='state' ) || adminUser(userId);
  },
  remove: function (userId, docs){
    return adminUser(userId) ;
    //|| _.all(docs, function(doc) {
      //return doc.owner === userId;
    //});
  }
});

#!/bin/bash

MONGOIMPORT='/usr/bin/mongoimport -d saljs  -c'
MONGOEXPORT='/usr/bin/mongoexport -h localhost:27017 -d saljs -c'
TARGETDIR='../backup'
SSHOPTIONS="-t -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

checkError()
{
 if [ "$?" -ne "0" ];
 then
     echo "Failed to : $1";
     exit  255;
 else
    echo "Managed to : $1";
 fi

}

backup()
{
   echo "Backingup to ../backup/${1}.json";
   ssh ${SSHOPTIONS} root@${TARGETHOST} "${MONGOEXPORT} ${1} -o ${TARGETDIR}/${1}.json" || checkError "Error - Export of ${1} data"
}

backupcollections()
{
  ssh ${SSHOPTIONS} root@${TARGETHOST} "mkdir -p ${TARGETDIR}"
  for col in $(cat ../col.txt)
   do
     backup ${col};
   done
}

downloadcollections()
{
  mkdir -p ${TARGETDIR}
  for col in $(cat ../col.txt)
   do
     rsync -e "ssh ${SSHOPTIONS}" root@${TARGETHOST}:${TARGETDIR}/${col}.json ./${TARGETDIR}/${col}.json   || checkError "Error - SCP target ${col}";
   done
}

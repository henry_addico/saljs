#!/bin/bash

TARGETHOST='app.rooneychase.com'
. ../importmongo.sh

MONGOIMPORT='mongoimport -h 127.0.0.1 --port 3001 -d meteor  -c'
MONGOCMD='mongo 127.0.0.1:3001/meteor --eval'

clearout()
{
   echo "Clearing out ${1}";
   ${MONGOCMD} "printjson(db.${1}.remove({}));"
}

importcollection()
{
   echo "Importing ${1}";
  ${MONGOIMPORT} ${1} ${TARGETDIR}/${1}.json
}


importcollections

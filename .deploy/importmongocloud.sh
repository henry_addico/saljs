#!/bin/bash
set -x
MONGOIMPORT='mongoimport --username meteorapps --password 8G4k37Ibd75O9saL mongodb+srv://cluster0.q9gws.mongodb.net/saljs -c'
CONTAINERREF='mongodb'
MONGOCMD='mongo --username meteorapps --password 8G4k37Ibd75O9saL mongodb+srv://cluster0.q9gws.mongodb.net/saljs --eval'
TARGETDIR='../backup'
SSHOPTIONS="-t -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"


checkError()
{
 if [ "$?" -ne "0" ];
 then
     echo "Failed to : $1";
     exit  255;
 else
    echo "Managed to : $1";
 fi
}

clearout()
{
   echo "Clearing out ${1}";
   ${MONGOCMD} "printjson(db.${1}.remove({}));"
}

importcollection()
{
   echo "Importing ${1}";
   ${MONGOIMPORT} ${1} ${TARGETDIR}/${1}.json
}


clearoutcollections()
{
  for col in $(cat ../col.txt)
   do
     clearout ${col};
   done
}

importcollections()
{
  for col in $(cat ../col.txt)
   do
     clearout ${col};
     importcollection ${col};
   done
}

uploadcollections()
{
  echo "ssh ${SSHOPTIONS} root@${TARGETHOST} "mkdir -p ${TARGETDIR}""
  ssh ${SSHOPTIONS} root@${TARGETHOST} "mkdir -p ${TARGETDIR}"
  for col in $(cat ../col.txt)
  do
     echo "Uploading ${SOURCE_BACKUP}/${TARGETDIR}/${col}.json ...";
     rsync -e "ssh ${SSHOPTIONS}" ${SOURCE_BACKUP}/${TARGETDIR}/${col}.json root@${TARGETHOST}:${TARGETDIR}/${col}.json || checkError "Error - SCP target ${col}";
     ssh ${SSHOPTIONS} root@${TARGETHOST} -- docker cp ${TARGETDIR}/${col}.json ${CONTAINERREF}:/tmp || checkError 'Error - Copy target to mongo container'
  done
}



module.exports = {
  servers: {
    one: {
      host: 'app.rooneychase.com',
      username: 'root',
      // password:
      // or leave blank for authenticate from ssh-agent
    }
  },

  meteor: {
    name: 'saljs',
    port: 3000,
    path: '../../',
    docker: {
      image: 'abernix/meteord:node-12-base',
    },
    servers: {
      one: {}
    },
    buildOptions: {
      serverOnly: true,
    },
    env: {
      ROOT_URL: "https://my.rooneychase.com",
      MONGO_URL: "mongodb://localhost:27017/saljs",
      MAIL_URL: "smtps://rooneychasebooking@gmail.com:pauladdico71@smtp.googlemail.com:465",
      ENVIRONMENT: "production"
    },

    deployCheckWaitTime: 60
  },
  proxy: {
    domains: 'my.rooneychase.com',
    ssl: {
      letsEncryptEmail: 'henry.addico@gmail.com',
      forceSSL: true,
    },
  },
  mongo: {
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },
};

variable "do_token" {
}

variable "pub_key" {
}

variable "pvt_key" {
}

variable "ssh_fingerprint" {
}

variable "app_version" {
}

variable "app" {
  type = bool
  default = true
}

provider "digitalocean" {
  version = "~> 1.6"
  token = var.do_token
}

provider "null" {
  version = "~> 2.1"
}


resource "digitalocean_droplet" "rooneychase-com-www" {
  count              = var.app ? 1 : 0
  image              = "ubuntu-20-04-x64"
  name               = "rooneychase-com-www"
  region             = "LON1"
  size               = "s-1vcpu-1gb"
  monitoring         = true
  private_networking = false
  ssh_keys = [
    var.ssh_fingerprint,
  ]

  connection {
    host        = "app.rooneychase.com"
    user        = "root"
    type        = "ssh"
    private_key = file(var.pvt_key)
    timeout     = "120m"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -v -i 'localhost,' dns.yml --extra-vars 'rax_ipv4_address=${self.ipv4_address} rax_app_name=app${ var.app==1 ? "" : var.app }'"
  }
  provisioner "remote-exec" {
    inline = ["echo '${self.name} has been created and ssh connectivity is working'"]
  }

}
resource "null_resource" "osdeps" {
  triggers = {
    cluster_instance_ids = "${join(",", digitalocean_droplet.rooneychase-com-www.*.id)}"
    app_hash = "${var.app_version}"
    config_hash = "${filemd5("middleware.yml")}"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -v -i '${join(",", digitalocean_droplet.rooneychase-com-www.*.ipv4_address)},' middleware.yml"
  }

  provisioner "local-exec" {
    working_dir = "digitalocean"
    command = "mup setup && mup deploy && bash ./runimport.sh"
  }
}

resource "digitalocean_droplet" "rooneychase-com-www2" {
  count              = var.app ? 1 : 0
  image              = "ubuntu-20-04-x64"
  name               = "rooneychase-com-www2"
  region             = "LON1"
  size               = "s-1vcpu-1gb"
  monitoring         = true
  private_networking = false
  ssh_keys = [
    var.ssh_fingerprint,
  ]

  connection {
    host        = "app2.rooneychase.com"
    user        = "root"
    type        = "ssh"
    private_key = file(var.pvt_key)
    timeout     = "120m"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -v -i 'localhost,' dns.yml --extra-vars 'rax_ipv4_address=${self.ipv4_address} rax_app_name=app2'"
  }
  provisioner "remote-exec" {
    inline = ["echo '${self.name} has been created and ssh connectivity is working'"]
  }

}

resource "null_resource" "osdeps2" {
  triggers = {
    cluster_instance_ids = "${join(",", digitalocean_droplet.rooneychase-com-www2.*.id)}"
    app_hash = "${var.app_version}"
    config_hash = "${filemd5("middleware.yml")}"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -v -i '${join(",", digitalocean_droplet.rooneychase-com-www2.*.ipv4_address)},' middleware.yml"
  }

  provisioner "local-exec" {
    working_dir = "digitalocean2"
    command = "mup setup && mup deploy && bash ./runimport.sh"
  }
}




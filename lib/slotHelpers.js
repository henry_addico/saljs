
const DURATION_IN_MINUTES = 30;
const STATE_FREE = "Free";
const STATE_RESERVED = 'Reserved';
const STATE_BOOKED = 'Booked';
const STATE_BLOCKED = 'Blocked';
const Months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
const NAVIGATION_KEY='navigationdate';

export function setRequiredDate(requiredDate){
  return Session.set(NAVIGATION_KEY, requiredDate.toDateString());
}
export function getRequiredDate(){
  return Session.get(NAVIGATION_KEY);
}

export function opad(value){
   if ( value == null ) {
     return "";
   }
  if(value < 10)
      return "0"+value;
  return value;
}

export class Booking {

  static get DURATION_IN_MINUTES(){
     return DURATION_IN_MINUTES;
  }
  static get STATE_FREE(){
     return STATE_FREE;
  }
  static get STATE_RESERVED(){
     return STATE_RESERVED;
  }
  static get STATE_BOOKED(){
     return STATE_BOOKED;
  }
  static get STATE_BLOCKED(){
     return STATE_BLOCKED;
  }
  static get Months(){
     return Months;
  }
  static get NAVIGATION_KEY(){
     return NAVIGATION_KEY;
  }
}

function canAdjustMonth(adjustMonthBy,sessionDate,currentDate){
    var change=false;
    if(Roles.userIsInRole(Meteor.userId(), ['admin'])){
        change=true;
    }else if(adjustMonthBy>0 ||  sessionDate.getMonth()> currentDate.getMonth()|| sessionDate.getFullYear() > currentDate.getFullYear()){
        change=true;
    }
    return change;
}

function adjustMonth(adjustMonthBy){
    var sessionDate = new Date(getRequiredDate());
    var currentDate = new Date();
    var change=canAdjustMonth(adjustMonthBy,sessionDate,currentDate);
    if(change){
        if(sessionDate.getDate() > 28){ sessionDate.setDate(28); }
        sessionDate.setMonth(sessionDate.getMonth()+adjustMonthBy);
        setRequiredDate(sessionDate);
    }
}
/**
 * Collapse slots of the default duration (DEFAULT_DURATION) into the duration of their appointments
 * @param slots - the array of slots with the same duration
 * @param date - for each group add the given date as a property
 * @returns {Array} - the results of the collapsing the original array
 */
function groupSlotsByAppointment(slots,date){
    groupedSlots = [];
    startOfGroup = null;
    for(index=0;index<slots.length-1;index++){
        if(startOfGroup == null && slots[index].appointment){
            startOfGroup = slots[index];
            startOfGroup.date=date;
            for(innerIndex=index+1;innerIndex<slots.length;innerIndex++){
                if(!slots[innerIndex].appointment || slots[innerIndex].appointment != startOfGroup.appointment ){
                    startOfGroup.end=slots[innerIndex-1].end;
                    groupedSlots.push(startOfGroup);
                    startOfGroup=null;
                    break;
                }else if(innerIndex==slots.length-1){
                    startOfGroup.end=slots[innerIndex].end;
                    groupedSlots.push(startOfGroup);
                    startOfGroup=null;
                }else{
                    index++;
                }
            }
        }else{
            slots[index].date=date;
            groupedSlots.push(slots[index]);
        }
    }
    return groupedSlots;
}


function createSlots(currentDate,durationInMinutes){
    slotConfig = ServiceConfig.findOne({'day':moment(currentDate).day()});
    slots = [];
    if(slotConfig && slotConfig.open){
        index = 0;
        slotEndTimeInMinutes=slotConfig.to.hour*60+(slotConfig.to.minute ? slotConfig.to.minute :0);
        startTimeInMinutes=slotConfig.from.hour*60+(slotConfig.from.minute ? slotConfig.from.minute :0);
        for(endTimeInMinutes=startTimeInMinutes+durationInMinutes; startTimeInMinutes<slotEndTimeInMinutes; startTimeInMinutes+=durationInMinutes,endTimeInMinutes+=durationInMinutes){
            var slotStartDate = moment.duration(startTimeInMinutes, 'minutes');
            var slotEndDate = moment.duration(endTimeInMinutes, 'minutes');
            slots.push({ 'user' : Meteor.userId(), 'index': index++, 'start' : {'hour':slotStartDate.hours(), 'minute':slotStartDate.minutes()}, 'end' : {'hour':slotEndDate.hours(), 'minute':slotEndDate.minutes()}, 'state' : Booking.STATE_FREE });
        }
    }
    return slots;
}




function getRequiredDuration(){
    var currentModel = model();
    if(currentModel && currentModel.serviceDetails && currentModel.serviceDetails.length){
        var totalDuration=0;
        for(var index=0;index<currentModel.serviceDetails.length;index++){
            totalDuration+=currentModel.serviceDetails[index].durationInMinutes;
        }
        return totalDuration;
    } else{
        return Booking.DURATION_IN_MINUTES;
    }
}
this.dayStringHelper = function () {
    var D = new Date(getRequiredDate());
    return "Slots for " + Months[D.getMonth()] + ", " + D.getDate();
}
this.dateStringHelper = function () {
    var D = new Date(getRequiredDate());
    return Months[D.getMonth()] + ", " + D.getFullYear();
}
this.getDaysHelper = function () {
    var sessionDate = new Date(getRequiredDate());
    var monthDate = new Date(sessionDate.getFullYear(), sessionDate.getMonth(), 1);
    var nextMonthDate = new Date(sessionDate.getFullYear(), sessionDate.getMonth() + 1, 0);
    var days = [
        {number: "Su", text: "Su"},
        {number: "Mo", text: "Mo"},
        {number: "Tu", text: "Tu"},
        {number: "We", text: "We"},
        {number: "Th", text: "Th"},
        {number: "Fr", text: "Fr"},
        {number: "Sa", text: "Sa"}
    ];
    for (var i = 0; i < monthDate.getDay(); i++) {
        days.push({text: "&nbsp;&nbsp;&nbsp;&nbsp;", number: i - monthDate.getDay()});
    }
    for (var i = 0; i < nextMonthDate.getDate(); i++) {
        if (sessionDate.getDate() == i + 1) {
            days.push({
                'number': i + 1,
                'text': (i + 1 < 10 ? "&nbsp;&nbsp;" + (i + 1) : i + 1),
                'activeClass': " active"
            });
        }
        else {
            days.push({
                'number': i + 1,
                'text': (i + 1 < 10 ? "&nbsp;&nbsp;" + (i + 1) : i + 1),
                'activeClass': ""
            });
        }
    }
    return days;
}
this.groupSlots = function (serviceSlots,duration,previousSlots){
    slots = serviceSlots.slots;

    groupedSlots = previousSlots ? previousSlots : [];
    startOfGroup = null;
    if(duration <= Booking.DURATION_IN_MINUTES){
        //do not support the breaking of slots into anything smaller
        //if the duration is the same as the current SLOT SIZE then
        //we simply return the the slot
        for(index=0;index<slots.length-1;index++){
            if(slots[index].state == Booking.STATE_FREE) {
                startOfGroup = slots[index];
                startOfGroup.date = serviceSlots.date;
                startOfGroup.id = serviceSlots._id;
                groupedSlots.push(startOfGroup);
            }
        }
        return groupedSlots
    }

    for(index=0;index<slots.length-1;index++){
        if(startOfGroup ==null && slots[index].state==Booking.STATE_FREE ){
            startOfGroup = slots[index];
            startOfGroup.date=serviceSlots.date;
            startOfGroup.id=serviceSlots._id;
            currentDuration=Booking.DURATION_IN_MINUTES;
            for(innerIndex=index+1;innerIndex<slots.length;innerIndex++){
                if(slots[innerIndex].state == Booking.STATE_FREE){
                    startOfGroup.end=slots[innerIndex].end;
                    currentDuration+=Booking.DURATION_IN_MINUTES;
                    index++;
                    if(currentDuration>=duration){
                        groupedSlots.push(startOfGroup);
                        startOfGroup=null;
                        break;
                    }
                }else{
                    startOfGroup=null;
                    break;
                }
            }
        }
    }
    return groupedSlots;
}
this.groupedSlotsForDate = function (endDate) {
    var slotsHandle = {slots: []};
    if(!endDate) {
        endDate = new Date(getRequiredDate());
    }
    var cdate = moment().hour(0).minutes(0).seconds(0);
    if( getValue("slotRange") == ".in2Months"){
        cdate = cdate.add(28, 'days');
    }
    var requiredDates = [];
    for (var startDate = cdate.toDate(); startDate.getTime() <= endDate.getTime(); startDate = moment(startDate).add(1, 'days').toDate()) {
        requiredDates.push(startDate.toDateString());
    }
    var serviceSlotCursor = ServiceSlots.find({'date': {$in: requiredDates}});

    serviceSlotCursor.forEach(function (serviceSlot) {
        var minimumSlotDuration = getRequiredDuration();
        if (minimumSlotDuration && serviceSlot) {
            slotsHandle.slots = groupSlots(serviceSlot, minimumSlotDuration, slotsHandle.slots);
        }
    });
    slotsHandle.slots = _.sortBy(slotsHandle.slots, function (givenSlot) {
        var dd = moment(new Date(givenSlot.date));
        return dd.toDate().getTime();
    });
    return slotsHandle;
}
this.slotsForDateHelper =  function () {
    serviceSlot = ServiceSlots.findOne({'date': getRequiredDate()});
    if (serviceSlot) {
        serviceSlot.slots = groupSlotsByAppointment(serviceSlot.slots, getRequiredDate());
    }
    return serviceSlot;
}
this.addSlot = function (onDate,force,bottom){
    if(!onDate)
        onDate=getRequiredDate();
    if(force == undefined || force == null)
        force=true;
    if(bottom == undefined || bottom == null)
        bottom = true;
    var currentDate = new Date(onDate);
    var serviceSlot = ServiceSlots.findOne({'date': onDate});
    if(serviceSlot){
        if(serviceSlot.slots.length < 1){
            serviceSlot.slots = createSlots(currentDate, Booking.DURATION_IN_MINUTES);
        }else if(force){
            if(bottom){
                var startTimeInMinutes=serviceSlot.slots[serviceSlot.slots.length-1].end.hour*60+(serviceSlot.slots[serviceSlot.slots.length-1].end.minute ? serviceSlot.slots[serviceSlot.slots.length-1].end.minute :0);
                var slotStartDate = moment.duration(startTimeInMinutes,'minutes');
                var slotEndDate = moment.duration(startTimeInMinutes+Booking.DURATION_IN_MINUTES,'minutes');
                serviceSlot.slots.push({ 'user' : Meteor.userId(), 'index': serviceSlot.slots.length,  'start' : {'hour':slotStartDate.hours(), 'minute':slotStartDate.minutes()}, 'end' : {'hour':slotEndDate.hours(), 'minute':slotEndDate.minutes()}, 'state' : Booking.STATE_FREE });

            }else{
                var startTimeInMinutes=serviceSlot.slots[0].start.hour*60+(serviceSlot.slots[0].start.minute ? serviceSlot.slots[0].start.minute :0);
                var slotStartDate = moment.duration(startTimeInMinutes-Booking.DURATION_IN_MINUTES,'minutes');
                var slotEndDate = moment.duration(startTimeInMinutes,'minutes');
                for(var j=0;j<serviceSlot.slots.length;j++){
                    serviceSlot.slots[j].index=j+1;
                }
                serviceSlot.slots.unshift({ 'user' : Meteor.userId(), 'index': 0,  'start' : {'hour':slotStartDate.hours(), 'minute':slotStartDate.minutes()}, 'end' : {'hour':slotEndDate.hours(), 'minute':slotEndDate.minutes()}, 'state' : Booking.STATE_FREE });

            }
        }
        if(serviceSlot.slots.length >1){
            ServiceSlots.update(serviceSlot._id,{$set:{slots:serviceSlot.slots,'open':true, 'availableSlotsCount':serviceSlot.slots.length+1}});
        }
    }else{
        var slots = createSlots(currentDate,Booking.DURATION_IN_MINUTES);
        ServiceSlots.insert({ 'user' : Meteor.userId(), 'date': onDate,'slots':slots, 'open': slots.length>0, 'availableSlotsCount':slots.length+1});
    }
}

this.toggleBlockingOfSlot =  function(event){
    var currentDate = new Date(getRequiredDate());
    var serviceSlot = ServiceSlots.findOne({'date': getRequiredDate()});
    var changed = false;
    if(serviceSlot){
        if(this.state==Booking.STATE_FREE){
            this.state=Booking.STATE_BLOCKED;
            changed=true;
        }else if(this.state==Booking.STATE_BLOCKED){
            this.state=Booking.STATE_FREE;
            changed=true;
        }
        if(changed){
            serviceSlot.slots[this.index] = this;
            ServiceSlots.update(serviceSlot._id,{$set:{slots:serviceSlot.slots}});
        }
    }
}

this.reservationOfSlot =  function(event){
    var currentDate = new Date(getRequiredDate());
    var serviceSlot = ServiceSlots.findOne({'date': getRequiredDate()});
    var changed = false;
    if(serviceSlot){
        if(this.state==Booking.STATE_FREE){
            this.state=Booking.STATE_RESERVED;
            changed=true;
        }else if(this.state==Booking.STATE_RESERVED){
            this.state=Booking.STATE_FREE;
            changed=true;
        }
        if(changed){
            serviceSlot.slots[this.index] = this;
            ServiceSlots.update(serviceSlot._id,{$set:{slots:serviceSlot.slots}});
        }
    }
}
export let undoBooking = function(selectedSlot){
    var serviceSlot = ServiceSlots.findOne({'date': selectedSlot.date});
    var changed = false;
    for(var index=selectedSlot.index;index<serviceSlot.slots.length
    && (serviceSlot.slots[index].end.hour < selectedSlot.end.hour
    || (serviceSlot.slots[index].end.hour == selectedSlot.end.hour
    && serviceSlot.slots[index].end.minute <= selectedSlot.end.minute))
    || (serviceSlot.slots[index].previous 
    && serviceSlot.slots[index].previous.appointment 
    && index > 0
    && serviceSlot.slots[index-1].appointment
    && serviceSlot.slots[index].previous.appointment == serviceSlot.slots[index-1].appointment);index++) {
        if(serviceSlot.slots[index].state == Booking.STATE_BOOKED || serviceSlot.slots[index].state == Booking.STATE_RESERVED){
            serviceSlot.slots[index].previous = _.clone(serviceSlot.slots[index]);
            serviceSlot.slots[index].state = Booking.STATE_FREE;
            delete serviceSlot.slots[index].appointment;
            delete serviceSlot.slots[index].contactName;
            delete serviceSlot.slots[index].contactNumber;
            delete serviceSlot.slots[index].date;
            delete serviceSlot.slots[index].description;
            changed=true;
        }else if(serviceSlot.slots[index].previous) {
            serviceSlot.slots[index] = _.clone(serviceSlot.slots[index].previous);
            changed = true;
        }
    }
    if(changed){
        ServiceSlots.update(serviceSlot._id,{$set:{slots:serviceSlot.slots}});
    }
}

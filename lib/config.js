import { AccountsTemplates } from 'meteor/useraccounts:core';

let contactNameField = {
  _id: 'contactName',
  type: 'text',
  displayName: 'Who should we ask for, should we need to call?',
  required: true
};

let contactNumberField = {
  _id: 'contactNumber',
  type: 'text',
  displayName: 'A tel or mobile number to contact you on',
  required: true
};

AccountsTemplates.configure({
  defaultLayout: 'layout',
  confirmPassword: true,
  enablePasswordChange: true,
  forbidClientAccountCreation: false,
  overrideLoginErrors: true,
  sendVerificationEmail: true,
  enforceEmailVerification: true,
  lowercaseUsername: false,
  showAddRemoveServices: false,
  showForgotPasswordLink: true,
  showLabels: true,
  showPlaceholders: true,
  continuousValidation: false,
  negativeFeedback: false,
  negativeValidation: true,
  positiveValidation: true,
  positiveFeedback: true,
  showValidating: true,
  privacyUrl: '/privacy',
  termsUrl: '/terms',
  homeRoutePath: '/',
  redirectTimeout: 4000
});
let extraFields = [contactNameField,contactNumberField,AccountsTemplates.removeField('email'), AccountsTemplates.removeField('password')];
AccountsTemplates.addFields(extraFields);
T9n.setLanguage('en');

AccountsTemplates.configureRoute('signIn');
AccountsTemplates.configureRoute('signUp');
AccountsTemplates.configureRoute('forgotPwd');
AccountsTemplates.configureRoute('resetPwd');

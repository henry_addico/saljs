import { moment } from 'meteor/momentjs:moment';
import { Meteor } from 'meteor/meteor'
import ical  from 'ical-generator';


var dateModel = {hour: Number, minute: Number};
export const AppointmentModel = {
      _id: Match.Optional(String),
      contactNumber: String,
      contactName: Match.Optional(String),
      durationInMinutes:Match.Optional(String),
      serviceDetails: [Match.ObjectIncluding({name:String,description:String,durationInMinutes:Number,priceFrom:Number })],
      slotStart: String,
      slotEnd: String,
      slotDate: String,
      slotId: String,
      payments: Match.Optional([Match.ObjectIncluding({ref:String,paymentType:String})]),
      emailAddress: Match.Optional(String),
      slotIndex: Number,
      userId: Match.Optional(String),
      slotRange: Match.Optional(String),
      selectedSlot:Match.ObjectIncluding({user:String,index:Number,state:String, date:String,id:String,start:Match.ObjectIncluding(dateModel),end:Match.ObjectIncluding(dateModel)})
};

export const updateSlot = function(targetSlot, givenAppointment) {
    targetSlot.state='Booked';
    targetSlot.appointment=givenAppointment._id;
    targetSlot.contactName=givenAppointment.contactName;
    targetSlot.contactNumber=givenAppointment.contactNumber;
    targetSlot.description= _.reduce(givenAppointment.serviceDetails, function(memo, serviceDetail){
        if(_.isEmpty(memo))
            return serviceDetail.groupDescription+"-"+serviceDetail.description;
        else
            return memo+", "+serviceDetail.groupDescription+"-"+serviceDetail.description;
    },"");
};

export const sendAppoinmentMail = function(targetSlot, givenAppointment) {
    try{
        const mailOptions = {
              to: givenAppointment.emailAddress,
              subject: "Confirmation of appointment for " + givenAppointment.contactName,
              heading: "Thanks "+ givenAppointment.contactName+", your appointment has been booked. ",
              message:"",
              buttonText: 'Full details',
              buttonUrl: 'http://my.rooneychase.com/thanks/'+givenAppointment._id,
              showFooter: true,
          };
          mailOptions.headingSmall = '<p>Your appointment is for '+targetSlot.description  +
          ' on '+ givenAppointment.slotDate + ' ' + givenAppointment.slotStart + ' ' + ' at ' +  Meteor.settings.company.address+', '+ Meteor.settings.company.city+', '+ Meteor.settings.company.postalCode+'</p>'

          mailOptions.ejson = '\n\r<script                                                                type"application/ld+json">\n\r'+
                    '{\n\r'+
                      '"@context": "http://schema.org",\n\r'+
                      '"@type":                 "EventReservation",\n\r'+
                      '"reservationStatus": "http://schema.org/Confirmed",\n\r'+
                      '"reservationNumber":     "'+givenAppointment._id+'",\n\r'+
                      '"underName": {\n\r'+
                      '  "@type":               "Person",\n\r'+
                      '  "name":                "'+givenAppointment.contactName+'",\n\r'+
                      '  "email":                "'+givenAppointment.emailAddress+'"\n\r'+
                      '},\n\r'+
                      '"reservationFor": {\n\r'+
                      '  "@type":               "Event",\n\r'+
                      '  "name":                "'+mailOptions.subject+'",\n\r'+
                      '  "startDate":           "'+
                      moment(givenAppointment.slotDate + ' ' + givenAppointment.slotStart, 'ddd MMM DD YYYY hh:mm').format('YYYY-MM-DDThh:mm:ss')+'-'+
                      moment(givenAppointment.slotDate + ' ' + givenAppointment.slotEnd, 'ddd MMM DD YYYY hh:mm' ).format('hh:mm')+'",\n\r'+
                      '  "location": {\n\r'+
                      '    "@type":             "Place",\n\r'+
                      '    "name":              "'+ Meteor.settings.company.address +'",\n\r'+
                      '    "address": {\n\r'+
                      '    "@type":           "PostalAddress",\n\r'+
                      '      "streetAddress":   "'+ Meteor.settings.company.address+ '",\n\r'+
                      '      "addressLocality": "'+ Meteor.settings.company.city+'",\n\r'+
                      '"addressRegion": "GL",\n\r'+
                      '      "postalCode":      "'+ Meteor.settings.company.postalCode+'",\n\r'+
                      '      "addressCountry":  "UK"\n\r'+
                      '    }\n\r'+
                      '  }\n\r'+
                      '}\n\r'+
                    '}\n\r'+
                    '</script>\n\r'
          console.log(mailOptions.ejson);
          let cal = ical({});
          let calEvent = cal.createEvent(
            {
              summary:  'Appointment for '+targetSlot.description+ ' at '+ PrettyEmail.options.companyName,
              organizer: {
                name: givenAppointment.contactName,
                email:  PrettyEmail.options.companyEmail
              },
              location: Meteor.settings.company.address,
              start: moment(givenAppointment.slotDate + ' ' + givenAppointment.slotStart, 'ddd MMM DD YYYY hh:mm').toDate(),
              end: moment(givenAppointment.slotDate + ' ' + givenAppointment.slotEnd, 'ddd MMM DD YYYY hh:mm' ).toDate()
            }
          );
        calEvent.createAttendee({email: givenAppointment.emailAddress, name: givenAppointment.contactName})
        mailOptions.cal = cal.toString();
        PrettyEmail.send('call-to-action', mailOptions);
        cal = ical({});
        calEvent = cal.createEvent(
          {
            summary:  givenAppointment.contactName + ' ('+ givenAppointment.contactNumber+') for '+targetSlot.description,
            organizer: {
              name: givenAppointment.contactName,
              email:  PrettyEmail.options.companyEmail
            },
            location: Meteor.settings.company.address,
            start: moment(givenAppointment.slotDate + ' ' + givenAppointment.slotStart, 'ddd MMM DD YYYY hh:mm').toDate(),
            end: moment(givenAppointment.slotDate + ' ' + givenAppointment.slotEnd, 'ddd MMM DD YYYY hh:mm' ).toDate()
          }
        );

        calEvent.createAttendee({email: PrettyEmail.options.companyEmail, name: PrettyEmail.options.companyName})
        mailOptions.cal = cal.toString();
        mailOptions.to = Meteor.settings.company.calendarEmail;
        PrettyEmail.send('call-to-action',mailOptions);


     }catch(e){
         console.log("error sending email for"+givenAppointment);
         console.log(e);
     }
};
import {undoBooking, Booking} from '../lib/slotHelpers';
import { Blaze } from 'meteor/blaze';
import { Template } from 'meteor/templating';
import { moment } from 'meteor/momentjs:moment';
import { Meteor } from 'meteor/meteor'
import ical  from 'ical-generator';
import {loadBusiness, createCustomer, createInvoice} from './accountswaves';
import {AppointmentModel,updateSlot,sendAppoinmentMail} from './servermodel';

    Meteor.methods({
        bookAppointment: function(givenAppointment){
                try {
                    check(givenAppointment, AppointmentModel);
                }catch(e){
                    console.log(e);
                    throw new Meteor.Error(400, "Please fill in all required fields with appropriate values");
                }
                update =false;
                if(givenAppointment._id) {
                    update =true;
                    undoBooking(Appointments.findOne({_id:givenAppointment._id}).selectedSlot);
                    Appointments.update(givenAppointment._id, givenAppointment);

                }else {
                    givenAppointment._id = Appointments.insert(givenAppointment);
                }
                var serviceSlot = ServiceSlots.findOne({_id:givenAppointment.slotId});
                if(serviceSlot && serviceSlot.slots && serviceSlot.slots[givenAppointment.slotIndex]){
                  endHour = givenAppointment.selectedSlot.end.hour;
                  endMinute = givenAppointment.selectedSlot.end.minute;
                  for(index = givenAppointment.slotIndex; index < serviceSlot.slots.length && 
                    (serviceSlot.slots[index].end.hour < endHour || 
                      (serviceSlot.slots[index].end.hour == endHour && 
                        serviceSlot.slots[index].end.minute <= endMinute)); index++ ){

                    var targetSlot = serviceSlot.slots[index];
	                  if(targetSlot.state=='Free' || update){
                      updateSlot(targetSlot,givenAppointment);
	                    serviceSlot.slots[index] = targetSlot;
	                    ServiceSlots.update(givenAppointment.slotId,{$set:{slots:serviceSlot.slots}});
                      if(givenAppointment.emailAddress){
                        sendAppoinmentMail(targetSlot,givenAppointment);
                      }
	                  }else{
	                  	throw new Meteor.Error(400, "Service Slot starting at "+targetSlot.start.hour+"-"+targetSlot.start.minute+" and ending "+targetSlot.end.hour+"-"+targetSlot.end.minute+" is no longer available");
	                  }
	                }
                }
                //reduce count of available slots and save slots
              console.log("sent email for appointment",givenAppointment._id);
            return givenAppointment._id;
        },
        postInvoice: function (givenAppointmentId, actualPrice) {
          check(givenAppointmentId,String);
          check(actualPrice, Number);
          const appointment = Appointments.findOne({ _id: givenAppointmentId });
          let contact = Contacts.findOne({ contactNumber: appointment.contactNumber });
          if (!contact) {
            contact = {
              contactNumber: appointment.contactNumber,
              emailAddress: appointment.emailAddress,
              contactName: appointment.contactName,
            }
            Contacts.insert(contact);
            contact = Contacts.findOne({ contactNumber: appointment.contactNumber });
          }
          if (!contact.accountId) {
            createCustomer(appointment)
            .then(function(body){
              console.log(body);
              if(body.data.customerCreate.didSucceed) {
                contact.accountId = body.data.customerCreate.customer.id;
                Contacts.update(contact._id, contact);
              }
            }).catch(function (error){
              console.log(error);
            })
          } else {
            createInvoice(appointment, actualPrice, contact.accountId);
          }
          //loadBusiness();
          //createCustomer
          /* const HiveageAPI = new Hiveage(Meteor.settings.hiveage.domain, Meteor.settings.hiveage.apiKey);
          const appointment = Appointments.findOne({ _id: givenAppointmentId });
          let contact = Contacts.findOne({ contactNumber: appointment.contactNumber });
          if (!contact) {
            contact = {
              contactNumber: appointment.contactNumber,
              emailAddress: appointment.emailAddress,
              contactName: appointment.contactName,
            }
            Contacts.insert(contact);
            contact = Contacts.findOne({ contactNumber: appointment.contactNumber });
          }
          if (!contact.connectionId) {
            HiveageAPI.createConnection(buildConnectionModel(appointment))
              .then((response) => {
                contact.connectionId = response.network.id
                Contacts.update(contact._id, contact);
                createInvoice(HiveageAPI, appointment, actualPrice, contact.connectionId)
              })
              .catch((err) => {
                console.log(err);
              });
          } else {
            createInvoice(HiveageAPI, appointment, actualPrice, contact.connectionId);
          } */
          
        },
        getAppointment: function (appointmentId) {
          check(appointmentId,String);
          var appointmentModel = Appointments.findOne({_id:appointmentId});
          return appointmentModel;
        },
        updateAppointment: function (givenAppointment){
        	check(givenAppointment,{
                      contactName: String,
                      contactNumber: String,
                      serviceDetail: String,
                      serviceType: String,
                      slotStart: String,
                      slotEnd: String,
                      slotDate: String,
                      slotId: String,
                      emailAddress: String,
                      slotIndex: Number,
                      userId: Match.Optional(String)
            });
            Appointments.update(givenAppointment._id,givenAppointment);
            return givenAppointment;
        },
        updateServiceType: function (givenServiceType){
        	check(givenServiceType,{
                      name: String,
                      description: String,
                      durationInMinutes: Number,
                      priceFrom: Number,
                      groups: Match.Maybe([String]),
                      _id: Match.Optional(String)
            });
            ServiceTypes.update(givenServiceType._id, givenServiceType);
            return givenServiceType;
        },
        addServiceType: function (givenServiceType){
        	check(givenServiceType,{
                      name: String,
                      description: String,
                      durationInMinutes: Number,
                      priceFrom: Number,
                      groups: Match.Maybe([String]),
                      _id: Match.Optional(String)
            });
            delete givenServiceType._id;
            return ServiceTypes.insert(givenServiceType);
        },
        updateServiceGroup: function (givenServiceGroup){
        	check(givenServiceGroup,{
                      name: String,
                      description: String,
                      title: String,
                      allowMultiple: Boolean,
                      _id: Match.Optional(String)
            });
            ServiceGroups.update(givenServiceGroup._id, givenServiceGroup);
            return givenServiceGroup;
        },
        updateServiceConfig: function (givenServiceConfig){
        	check(givenServiceConfig,{
                      day: Number,
                      dayText: String,
                      from: Match.OneOf({hour: Number, minute: Number}, null),
                      to: Match.OneOf({hour: Number, minute: Number}, null),
                      open: Boolean,
                      _id: Match.Optional(String)
            });
            ServiceConfig.update(givenServiceConfig._id, givenServiceConfig);
            return givenServiceConfig;
        },
        addServiceGroup: function (givenServiceGroup){
        	check(givenServiceGroup,{
                      name: String,
                      description: String,
                      title: String,
                      allowMultiple: Boolean,
                      _id: Match.Optional(String)
            });
            delete givenServiceGroup._id;
            return ServiceGroups.insert(givenServiceGroup);
        },
        getSlotState: function (slotId, slotIndex) {
          check(slotId,String);
          check(slotIndex,Number);

          var serviceSlot = ServiceSlots.findOne({_id:slotId});
          var changed=false;
          if(serviceSlot && serviceSlot.slots && serviceSlot.slots[slotIndex]){
            var targetSlot = serviceSlot.slots[slotIndex];
            return targetSlot.state;
          }

           throw new Meteor.Error(400, "Service Slot does not exits"+slotId+" - "+ slotIndex);
        },
        reserveSlot: function (givenAppointment) {
            var dateModel = {hour: Number, minute: Number};
            var modelTemplate = {
                _id: Match.Optional(String),
                description: String,
                durationInMinutes:String,
                location: Match.Optional(String),
                emailAddress: Match.Optional(String),
                slotStart: String,
                slotEnd: String,
                slotDate: String,
                slotId: String,
                slotIndex: Number,
                userId: Match.Optional(String),
                slotRange: Match.Optional(String),
                selectedSlot:Match.ObjectIncluding({user:String,index:Number,state:String, date:String,id:String,start:Match.ObjectIncluding(dateModel),end:Match.ObjectIncluding(dateModel)})
            };

            check(givenAppointment,modelTemplate);

            if(givenAppointment._id)
                Appointments.update(givenAppointment._id,givenAppointment);
            else
                givenAppointment._id = Appointments.insert(givenAppointment);

            var serviceSlot = ServiceSlots.findOne({_id:givenAppointment.slotId});
            var changed=false;
            if(serviceSlot && serviceSlot.slots && serviceSlot.slots[givenAppointment.slotIndex]){
                endHour = givenAppointment.selectedSlot.end.hour;
                endMinute = givenAppointment.selectedSlot.end.minute;
                for(index = givenAppointment.slotIndex; index < serviceSlot.slots.length && (serviceSlot.slots[index].end.hour < endHour || (serviceSlot.slots[index].end.hour == endHour && serviceSlot.slots[index].end.minute <= endMinute)); index++ ){

                    var targetSlot = serviceSlot.slots[index];
                    if(targetSlot.state==Booking.STATE_FREE){
                        targetSlot.state=Booking.STATE_RESERVED;
                        targetSlot.appointment=givenAppointment._id;
                        targetSlot.contactName="Personal";
                        targetSlot.contactNumber="Reservation";
                        if(_.isEmpty(givenAppointment.location)) {
                            targetSlot.description = givenAppointment.description
                        }else {
                            targetSlot.description = givenAppointment.description + "-" + givenAppointment.location;
                        }
                        serviceSlot.slots[index] = targetSlot;
                        changed=true;
                    }else{
                        throw new Meteor.Error(400, "Service Slot starting at "+targetSlot.start.hour+"-"+targetSlot.start.minute+" and ending "+targetSlot.end.hour+"-"+targetSlot.end.minute+" is no longer available");
                    }
                }
                if(changed){
                    ServiceSlots.update(givenAppointment.slotId,{$set:{slots:serviceSlot.slots}});
                }
            }
            return givenAppointment._id;
        }


      });

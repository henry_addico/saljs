import Hiveage from 'hiveage';


const buildConnectionModel = function(appointment){
    return {
      "network": {
        "name": appointment.contactName,
        "first_name": appointment.contactName,
        "business_email": appointment.emailAddress,
        "category": 'individual',
        "language": 'en-uk',
        "currency": "GBP",
  
      }
    }
  };

  const buildInvoiceModel = function(appointment, actualPrice, connectionId){
    const selecteItems = [];
    appointment.serviceDetails.forEach((sel,index) => {
      selecteItems.push({
          "date": moment(appointment.slotDate,"ddd MMM DD YYYY").format('YYYY-MM-DD'),
          "description": sel.description,
          "price": sel.priceFrom,
          "quantity": "1",
          /*"unit": "hourly", not using unit at the moment*/ 
          "sort_order": index+1
        })
    });
    const invoice = {
      "invoice": {
          "connection_id": connectionId,
          "due_date": moment(appointment.slotDate, "ddd MMM DD YYYY").format('YYYY-MM-DD'),
          "date": moment().format('YYYY-MM-DD'),
          "summary": "Appointment for " + appointment.contactName,
          "note": "Appointment Starts at "+ appointment.slotDate + ' ' + appointment.slotStart ,
          "statement_no": "IN-"+appointment._id,
          "payment_options": ["paypal", "cash", "check"],
          "allow_partial_payments": 1,
          "send_receipts_automatically": 1,
          "send_reminders": false,
      
          /*"taxes_attributes": [{
            "name": "VAT",
            "key": 23,
            "combined_amount_percent": "5%",
            "percentage": true
          }, ],*/
      
          /*"discounts_attributes": [{
            "name": "DIS",
            "key": 145,
            "combined_amount_percent": "5",
            "percentage": false
          }, ],*/
      
          /*"shipping_amounts_attributes": [{
            "name": "DHL",
            "key": 205,
            "combined_amount_percent": "5",
            "percentage": false
          }, ],*/
      
          "items_attributes": selecteItems,
      
          /*"expenses_attributes": [{
            "date": "2015-10-18",
            "description": "Item descriptions",
            "amount": "10.00",
            "sort_order": 2
          },],*/
      
          /*"tasks_attributes": [{
            "date": "2015-10-18",
            "description": "Item descriptions",
            "duration": "120",
            "rate": "10.00",
            "sort_order": 3
          },],*/
      
          /*"trips_attributes": [{
            "date": "2015-10-18",
            "description": "Item descriptions",
            "distance": "2",
            "rate": "10.00",
            "sort_order": 4
          }]*/
        }
      };
      return invoice;
  }
  
  const createInvoice = function(HiveageAPI,appointment,actualPrice,connectionId) {
    HiveageAPI.createInvoice(buildInvoiceModel(appointment, actualPrice, connectionId))
    .then((response) => {
      console.log(response);
      let payments = [];
      if(appointment.payments){
        payments = appointment.payments
      }
      payments.push([{
        invoiceref:response.invoice.id,
        contactref:response.invoice.connection.id,
        contacthash:response.invoice.connection.hash_key,
      }]);
      appointment.payments = payments;
      Appointments.update(appointment._id, appointment);
    })
    .catch((err) => {
      console.log(err);
    });
  }
  

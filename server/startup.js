
function createServiceGroups(){
    ServiceGroups.insert({name:"washcutstyle", allowMultiple:false ,title:"Style", description: "Wash, cut, set, style, blow dry ..."});
    ServiceGroups.insert({name:"conditiontreatement", allowMultiple:false, title:"Condition", description: "Condition treatment, moisturising, exfoliate ..."});
    ServiceGroups.insert({name:"weaves", allowMultiple:false, title:"Weave", description: "Weaves, weave wig, half weave pick drop ..."});
    ServiceGroups.insert({name:"weaveadditionalServices",allowMultiple:true, title:"Weave Additional Services",description: "Services that can be done in addition to a weave service"});
    ServiceGroups.insert({name:"weavecare", allowMultiple:false,  title:"Weave care", description: "tightening, styling, wash, repair ..."});
    ServiceGroups.insert({name:"colours", allowMultiple:false, title:"Colour",description: "Hair colouring, pair foil, highlight ..."});
    ServiceGroups.insert({name:"relaxerpermkeratin", allowMultiple:false,title:"Relax", description: "Relax,Perm,Keratin ..."});
    ServiceGroups.insert({name:"chemicaltreatmentAdditionalServices", allowMultiple:true, title:"Additional Services for Chemical Treatment",description: "Services that can be done in addition to Relax,Perm,Keratin and Colour services"});
    ServiceGroups.insert({name:"braidsandtwist", title:"Braids", allowMultiple:false,description: "Braids, twist, corn roll ..."});
    ServiceGroups.insert({name:"bridal", title:"Bridal", allowMultiple:true, description: "Bridal consultation, prep, trial ..."});
}

function createServiceTypeWithGroups(services,group){
    console.log('Creating services: ');
    _.each(services,function(service){
        service = _.extend(service,{groups:[group]});
        ServiceTypes.insert(service);
    });
    console.log('Created services: ');
}

function createServiceTypes(){
    var services = [
        {name:"setblowdry",description:"Set & blow-dry",durationInMinutes:45, priceFrom:25},
        {name:"pressstyle",description:"Press & style",durationInMinutes:60, priceFrom:30},
        {name:"cutstyle",description:"Cut & Style",durationInMinutes:90,priceFrom:35},
        {name:"hairup",description:"Hair up",durationInMinutes:90,priceFrom:35},
        {name:"afrowashpressstyle",description:"Afro-wash/press",durationInMinutes:60,priceFrom:30},
        {name:"otherwashcutstyle",description:"Other",durationInMinutes:60,priceFrom:30}
        ];
    createServiceTypeWithGroups(services,"washcutstyle");
    services = [
        {name:"moisturising",description:"Mositurising",durationInMinutes:120, priceFrom:30},
        {name:"flakecontrolmoisturiser",description:"Flake control & moisturiser",durationInMinutes:150, priceFrom:35},
        {name:"proteinplusmoisturising",description:"Protein plus moisturising",durationInMinutes:150, priceFrom:35},
        {name:"otherconditiontreatement",description:"Other",durationInMinutes:120,priceFrom:""}
    ];
    createServiceTypeWithGroups(services,"conditiontreatement");

    services = [
        {name:"fullheadlong",description:"Full head long",durationInMinutes:180, priceFrom:70},
        {name:"fullheadshort",description:"Full head short",durationInMinutes:210, priceFrom:75},
        {name:"threequaterheadlong",description:"3/4 head long",durationInMinutes:180, priceFrom:70},
        {name:"threequaterheadshort",description:"3/4 head short",durationInMinutes:180, priceFrom:70},
        {name:"halfweavepickdrop",description:"1/2 weave pick and drop",durationInMinutes:40, priceFrom:70},
        {name:"weavewig",description:"Wig",durationInMinutes:180, priceFrom:85},
        {name:"otherweave",description:"Other",durationInMinutes:180,priceFrom:70}
    ]
    createServiceTypeWithGroups(services,"weaves");
    services = [
        {name:"weaveadditionalweavetakeout",description:"Additional Weave Takeout",durationInMinutes:30, priceFrom:10},
        {name:"weaveadditionalweavetracking",description:"Additional Weave-Tracking per roll",durationInMinutes:20, priceFrom:10},
        {name:"weaveadditionalwash",description:"Additional Wash",durationInMinutes:10, priceFrom:10},
        {name:"weaveadditionaltrim",description:"Additional Trim",durationInMinutes:5, priceFrom:5},
        {name:"weaveadditionaltreatment",description:"Additional Treatment",durationInMinutes:20, priceFrom:15},
        {name:"weaveadditionalsemicolour",description:"Additional Semi-colour",durationInMinutes:30, priceFrom:15},
        {name:"weaveadditionalscalpexfoliate",description:"Additional scalp exfoliate",durationInMinutes:20, priceFrom:5},
        {name:"weaveadditionalreleaxhairline",description:"Additional Relax hairline",durationInMinutes:30, priceFrom:20}
    ]

    createServiceTypeWithGroups(services,"weaveadditionalServices");
    services = [
        {name:"tighteningandstyle",description:"Tightening & style",durationInMinutes:60, priceFrom:5},
        {name:"washtighteningandstyle",description:"Wash, tightening & style",durationInMinutes:150, priceFrom:5},
        {name:"washandrepair",description:"Wash & repair",durationInMinutes:180, priceFrom:5},
        {name:"otherweavecare",description:"Other",durationInMinutes:150,priceFrom:5}
    ]
    createServiceTypeWithGroups(services,"weavecare");
    services = [
        {name:"relaxervirgin",description:"Relaxer-virgin",durationInMinutes:150, priceFrom:60},
        {name:"relaxerregrowth",description:"Relaxer-regrowth",durationInMinutes:120, priceFrom:50},
        {name:"permvirgin",description:"Perm-virgin",durationInMinutes:180, priceFrom:65},
        {name:"permregrowth",description:"Perm-regrowth",durationInMinutes:150, priceFrom:55},
        {name:"keratinstraightening",description:"Keratin straightening",durationInMinutes:120, priceFrom:85},
        {name:"otherrelax",description:"Other",durationInMinutes:150,priceFrom:50}
    ];

    createServiceTypeWithGroups(services,"relaxerpermkeratin");
    services = [
        {name:"permanentregrowth",description:"Permanent-regrowth",durationInMinutes:120, priceFrom:45},
        {name:"Permanentfullhead",description:"Permanent-full head",durationInMinutes:150, priceFrom:50},
        {name:"Highlighthalfhead",description:"Highlight-half head",durationInMinutes:150, priceFrom:55},
        {name:"fullhead",description:"Full head",durationInMinutes:180, priceFrom:60},
        {name:"tsection",description:"T-section",durationInMinutes:120, priceFrom:45},
        {name:"pairfoil",description:"Pairfoil",durationInMinutes:30, priceFrom:3},
        {name:"semicolour",description:"Semi colour",durationInMinutes:150, priceFrom:30},
        {name:"othercolours",description:"Other",durationInMinutes:180,priceFrom:60}
    ];
    createServiceTypeWithGroups(services,"colours");
    services = [
        {name:"chemicaltreatmentadditionalweavetracking",description:"Additional Weave-Tracking per roll",durationInMinutes:20, priceFrom:10},
        {name:"chemicaltreatmentadditionaltrim",description:"Additional Trim",durationInMinutes:10, priceFrom:10},
        {name:"chemicaltreatmentadditionalcut",description:"Additional cut",durationInMinutes:30, priceFrom:20},
        {name:"chemicaltreatmentadditionaltreatment",description:"Additional Treatment",durationInMinutes:20, priceFrom:15},
        {name:"chemicaltreatmentadditionalsemicolour",description:"Additional Semi-colour",durationInMinutes:30, priceFrom:15}
    ]
    createServiceTypeWithGroups(services,"chemicaltreatmentAdditionalServices");
    services = [
        {name:"braidsmall",description:"Small", durationInMinutes:300, priceFrom:80},
        {name:"braidmedium",description:"Medium",durationInMinutes:240, priceFrom:70},
        {name:"braidjumbo",description:"Big/jumbo",durationInMinutes:240, priceFrom:60},
        {name:"pickdropsmall",description:"Pick & Drop small",durationInMinutes:240, priceFrom:70},
        {name:"pickdropmdedium",description:"Pick & Drop medium",durationInMinutes:240, priceFrom:60},
        {name:"cornroll",description:"Corn roll",durationInMinutes:180, priceFrom:30},
        {name:"ghanabraids",description:"Ghana braids", durationInMinutes:240, priceFrom:40},
        {name:"naturalhairtwist",description:"Natural hair twist",durationInMinutes:60, priceFrom:20},
        {name:"otherbraids",description:"Other",durationInMinutes:240,priceFrom:40}
    ];


    createServiceTypeWithGroups(services,"braidsandtwist")
    services = [
        {name:"bridalconsultation",description:"Consultation",durationInMinutes:1, isFree: true, priceFrom:0},
        {name:"bridaltrial",description:"Trial",durationInMinutes:1, priceFrom:35},
        {name:"bridalbrideprep",description:"Bride Prep night/day",durationInMinutes:1, priceFrom:150},
        {name:"bridalbridemaid",description:"Bride maid Prep each",durationInMinutes:1, priceFrom:33}
    ]
    createServiceTypeWithGroups(services,"bridal");
    services = [
        {name:"additionalweavetakeout",description:"Additional Weave Takeout",durationInMinutes:30, priceFrom:10},
        {name:"additionalweavetracking",description:"Additional Weave-Tracking per roll",durationInMinutes:20, priceFrom:10},
        {name:"additionalwash",description:"Additional Wash",durationInMinutes:10, priceFrom:10},
        {name:"additionaltrim",description:"Additional Trim",durationInMinutes:5, priceFrom:5},
        {name:"additionalcut",description:"Additional cut",durationInMinutes:30, priceFrom:20},
        {name:"additionaltreatment",description:"Additional Treatment",durationInMinutes:20, priceFrom:15},
        {name:"additionalsemicolour",description:"Additional Semi-colour",durationInMinutes:30, priceFrom:15},
        {name:"additionalscalpexfoliate",description:"Additional scalp exfoliate",durationInMinutes:20, priceFrom:5},
        {name:"additionalreleaxhairline",description:"Additional Relax hairline",durationInMinutes:30, priceFrom:20},
        {name:"additionalreleaxhairline",description:"Additional Relax hairline",durationInMinutes:30, priceFrom:20}
    ]
    createServiceTypeWithGroups(services,"additionalServices");
}
createServiceConfig = function(){
    ServiceConfig.insert({day:1, dayText:"Monday", from:{'hour':9, 'minute': 30}, to:{'hour':14, 'minute': 30}, open: true});
    ServiceConfig.insert({day:2, dayText:"Tuesday", from:{'hour':9, 'minute': 30}, to:{'hour':14, 'minute': 30}, open: true});
    ServiceConfig.insert({day:3, dayText:"Wednesday", from:{'hour':9, 'minute': 30}, to:{'hour':14, 'minute': 30}, open: true});
    ServiceConfig.insert({day:4, dayText:"Thursday", from:{'hour':9, 'minute': 30}, to:{'hour':17, 'minute': 0}, open: true});
    ServiceConfig.insert({day:5, dayText:"Friday", from:{'hour':9, 'minute': 30}, to:{'hour':17, 'minute': 0}, open: true});
    ServiceConfig.insert({day:6, dayText:"Saturday", from:{'hour':8, 'minute': 0}, to:{'hour':20,'minute': 0}, open: true});
    ServiceConfig.insert({day:0, dayText:"Sunday", open: false});
}
createTestUsers =  function(){
    console.log('Creating users: ');
    var users = [
        { email:"teiaddico@hotmail.com",roles:[]},
        { email:"henry.addico@gmail.com",roles:['admin']}
    ];
    _.each(users, function (userData) {
        var id, user;
        console.log(userData);
        id = Accounts.createUser({
            email: userData.email,
            password: "apple1",
            profile: { firstName:"Henry", lastName:"Addico", mobileTel:"07738370760" }
        });
        // email verification
        Meteor.users.update({_id: id}, {$set:{'emails.0.verified': true}});
        Roles.addUsersToRoles(id, userData.roles);
    });
    console.log('Created users');
}
configureAccounts = function(){
    Accounts.validateNewUser(function (user) {
        var loggedInUser = Meteor.user();
        console.log(loggedInUser);
        console.log(user);
        if(!loggedInUser && user && user.services && (user.services.google||user.services.facebook)){
            return true;
        }
        if (Roles.userIsInRole(loggedInUser, ['admin'])) {
            return true;
        }

        if(loggedInUser) {
            throw new Meteor.Error(403, "Not authorized to create another account");
        }

        return true;
    });
}
publishSubscriptions = function(){
    Meteor.publish("serviceTypes", function () {
        return ServiceTypes.find({});
    });
    Meteor.publish("serviceGroups", function () {
        return ServiceGroups.find({});
    });
    Meteor.publish("serviceConfig", function () {
        return ServiceConfig.find(
            {},
            {sort: { day: 1 }}
            );
    });
    Meteor.publish("appointments", function () {
        return Appointments.find({});
    });
    Meteor.publish("serviceSlots", function () {
        if (this.userId && Roles.userIsInRole(this.userId,['admin'])){
            return ServiceSlots.find({});
        }else{
            var cdate = moment().hour(0).minutes(0).seconds(0);
            endDate = moment().add(168,'days').toDate();//8 weeks
            slotsHandle={slots:[]}
            requiredDates = [];
            for(var startDate=cdate.toDate(); startDate.getTime()<=endDate.getTime();startDate = moment(startDate).add(1, 'days').toDate()){
                requiredDates.push(startDate.toDateString());
            }
            return ServiceSlots.find({'date': {$in: requiredDates}});
        }
    });


}
configureEmails = function(){
    PrettyEmail.options = {
        from: 'enquiries@rooneychase.com',
        logoUrl: 'https://scontent-lhr3-1.xx.fbcdn.net/hphotos-xap1/v/t1.0-9/s720x720/1536662_632935743445510_1701568996745259662_n.png?oh=f726600804e329de05ba5aa8a93aa801&oe=567F6CC5',
        companyName: 'RooneyChase',
        companyUrl: 'http://rooneychase.com',
        companyAddress: Meteor.settings.company.address, 
        companyTelephone: '07958 640 393',
        companyEmail: Meteor.settings.company.email,
        siteName: 'my.rooneychase.com',
        facebook:'https://www.facebook.com/rooneychase',
        twitter:'https://twitter.com/rooneychase',
        website:'http://rooneychase.com',
        showFollowBlock:true,
        showFollowBlock:true

    };
    PrettyEmail.send = function(template,options) {
      const mailOptions = _.extend( {}, PrettyEmail.options, options);
      const mailModel = {
          from: options.from,
          to: options.to,
          subject: options.subject,
          html: PrettyEmail.render(template, options)
      };

      if(mailOptions.cal){
        mailModel.icalEvent = {content: mailOptions.cal};
      }
      if(mailOptions.ejson){
        mailModel.html = mailModel.html.replace('<head>', '<head>'+mailOptions.ejson);
      }
      Email.send(mailModel);
    };
}
Meteor.startup(function () {
    if (ServiceGroups.find().count() < 1) {
        createServiceGroups();
    }
    if (ServiceTypes.find().count() < 1) {
        createServiceTypes();
    }
    if (ServiceConfig.find().count() < 1) {
        createServiceConfig();
    }

    if (Meteor.users.find().count() < 1 ) {
        createTestUsers();
    }
    configureAccounts();
    configureEmails();
    publishSubscriptions();
});

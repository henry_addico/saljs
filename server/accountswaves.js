import { createHttpLink } from 'apollo-link-http';
import fetch from 'node-fetch';
import gql from 'graphql-tag';
import ApolloClient from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';


const createWavesClient = function () {
    const endpoint = Meteor.settings.waves.url;
    const urlLink = createHttpLink ({ uri: endpoint, fetch});
    const client = new ApolloClient({
      link: urlLink,
      cache: new InMemoryCache()
    })
    return client;
  };

  export const createCustomer = function(appointment) {
    const client = createWavesClient();
    const customerMutation = gql
`mutation ($input: CustomerCreateInput!) {
    customerCreate(input: $input) {
      didSucceed
      inputErrors {
        code
        message
        path
      }
      customer {
        id
        name
        firstName
        lastName
        email
        address {
          addressLine1
          addressLine2
          city
          province {
            code
            name
          }
          country {
            code
            name
          }
          postalCode
        }
        currency {
          code
        }
      }
    }
  }`;
    return client.mutate({
        mutation: customerMutation,
        variables: {
            "input": {
                "businessId": Meteor.settings.waves.businessId,
                "name": appointment.contactName,
                "firstName": appointment.contactName,
                "lastName": appointment.contactNumber,
                "phone":  appointment.contactNumber,
                "email": appointment.emailAddress,
                "currency": "GBP"
              }
        },
        context: {
          headers: {
            Authorization: `Bearer ${Meteor.settings.waves.token}`,
          }
        }
      })
  };
  const createInvoice = function(appointment,actualPrice,customerId) {
    const client = createWavesClient();
    const invoiceMutation = gql
`mutation ($input: InvoiceCreateInput!) {
    invoiceCreate(input: $input) {
      didSucceed
      inputErrors {
        code
        message
        path
      }
      customer {
        id
        name
        firstName
        lastName
        email
        address {
          addressLine1
          addressLine2
          city
          province {
            code
            name
          }
          country {
            code
            name
          }
          postalCode
        }
        currency {
          code
        }
      }
    }
  }`;
  const selecteItems = [];
    appointment.serviceDetails.forEach((sel,index) => {
      selecteItems.push({
          "date": moment(appointment.slotDate,"ddd MMM DD YYYY").format('YYYY-MM-DD'),
          "description": sel.description,
          "price": sel.priceFrom,
          "quantity": "1",
          /*"unit": "hourly", not using unit at the moment*/ 
          "sort_order": index+1
        })
    });
    return client.mutate({
        mutation: invoiceMutation,
        variables: {
            "input": {
                "businessId": Meteor.settings.waves.businessId,
                "customerId": customerId,
                "due_date": moment(appointment.slotDate, "ddd MMM DD YYYY").format('YYYY-MM-DD'),
                "date": moment().format('YYYY-MM-DD'),
                "summary": "Appointment for " + appointment.contactName,
                "note": "Appointment Starts at "+ appointment.slotDate + ' ' + appointment.slotStart ,
                "statement_no": "IN-"+appointment._id,
                "payment_options": ["paypal", "cash", "check"],
                "allow_partial_payments": 1,
                "send_receipts_automatically": 1,
                "send_reminders": false,
                "items": selecteItems,
              }
        },
        context: {
          headers: {
            Authorization: `Bearer ${Meteor.settings.waves.token}`,
          }
        }
      })
  }
  export const loadBusiness = function() {
    const client = createWavesClient();
    const bizQuery = gql`query {
        businesses(page: 1, pageSize: 10) {
          pageInfo {
            currentPage
            totalPages
            totalCount
          }
          edges {
            node {
              id
              name
              isClassicAccounting
              isClassicInvoicing
              isPersonal
            }
          }
        }
      }` 
    client.query({
      query: bizQuery,
      context: {
        headers: {
          Authorization: `Bearer ${Meteor.settings.waves.token}`,
        }
      }
    }).then(function(body){
        body.data.businesses.edges.forEach((sel,index) => {
            console.log(sel);
        });
    }).catch(function (error){
      console.log(error);
    })
  };